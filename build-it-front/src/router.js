import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Login from "./components/auth/Login";
import auth from "./components/auth/auth";

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/',
            name: 'home',
            component: Home,
            beforeEnter: (to, from, next) => {
                if (!auth.hasAuth()) {
                    next({path: '/login'});
                } else {
                    next();
                }
            }
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/logout',
            name: 'logout',
            component: Login,
            beforeEnter: (to, from, next) => {
                auth.logout();
                next({path: '/login'});
            }
        }
    ]
})
