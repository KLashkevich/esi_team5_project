import axios from "axios";

export default {
    user: { roles: [], username: "", authenticated: false },
    login: function (context, username, password, redirect) {
        let token = btoa(username+":"+password);

        axios.get("http://localhost:8080/api/authenticate", {headers: {'Authorization': `Basic ${token}`}})
            .then(response => {
                this.username = username;
                this.user.roles = response.data;
                this.user.authenticated = true;
                window.localStorage.setItem('token-'+this.username, token);
                if (redirect)
                    context.$router.push({path: redirect});
            })
            .catch(error => {
                console.log(error);
            });
    },
    hasAnyOf: function(roles) {
        return this.user.roles.find(role => roles.includes(role));
    },
    logout: function() {
        window.localStorage.removeItem('token-'+this.username);
        this.user = { roles: [], username: "", authenticated: false };
    },
    hasAuth: function() {
        return this.user.authenticated;
    },
    getAuthHeader: function() {
        return {
            'Authorization': 'Basic ' + window.localStorage.getItem('token-'+this.username)
        }
    }
}
