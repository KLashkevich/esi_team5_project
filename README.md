# ESI Project
See the project report and other documentation in `Report.pdf`.

The solution contains of four projects:
### RentIt
Java Spring Boot back-end application providing REST API to work with plants and purchase orders. In dev mode runnung on [`localhost:8090`](http://localhost:8090/).
### rentit-front
VueJS front-end for RentIt. Currently contains minimal functionality - accept / reject existing purchase orders. To run it, do to its folder, run `npm install` and `npm run serve`. 
Running on [`localhost:4010`](http://localhost:4010/).
### BuildIt
Java Spring Boot back-end application providing REST API for handling plant hire requests. 
In dev mode runnung on [`localhost:8080`](http://localhost:8080/).
### build-it-front
VueJS front-end for BuildIt. Currently allows to query existing available plants, create plant hire requests and approve or reject them. To run it, do to its folder, run `npm install` and `npm run serve`. 
Running on [`localhost:4000`](http://localhost:4000/).

### Docker and Deployment
The dockerization infrastructure (proper projects configuration, dockerfiles, docker-compose configuration etc.) is configured in the branch `build_configuration` of our repository, which contains all the latest changes from the master branch.

The docker configuration is the following:
- Each project has Dockerfile inside their roots (4 dockerfiles in total);
- There are no hard-coded links inside the services: all the links are ingested as environment variables at the time of containers’ composition (docker-compose);
- In order to deploy all the containers locally, we have docker-compose-local.yml file in the repository root. All the containers configuration and local deployment can be done by simply executing deploy.ps1 script in the root of the repository. The core prerequisite for local deployment is that Docker should be installed on the machine;
- For the live deployment we use docker-compose.yml, which is configured for remote links. To build the updated containers and deploy them to live environment we use ‘docker-compose up --build -d’ command when attached to remote docker machine.
### Running cucumber tests
All of the features are located at /BuildIt/src/test/resources/features. In the beginning of each .feature file, the covered CC and PS are commented in after the feature description.
In order to run acceptance tests, do the following:
1) Run both REST API applications (BuildIt and RentIt)
2) Manually run the contents of RentIt’s test/resources/plants-dataset.sql file to in h2 console of RentIt API (http://localhost:8090/h2). 
3) Run both front-end apps (rentit-front and build-it-front)
4) Change the path to the location of the installed chrome driver in PlantCatalogSteps.java (if needed).
5) Run tests by running CucumberTest.java or run each .feature file separately


# Made by Team 5:
- Kaarel Sõrmus
- Kiryl Lashkevich
- Joosep Tenn
- Sander Sõritsa