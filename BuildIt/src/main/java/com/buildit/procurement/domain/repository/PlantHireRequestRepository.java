package com.buildit.procurement.domain.repository;

import com.buildit.procurement.domain.model.PlantHireRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantHireRequestRepository extends JpaRepository<PlantHireRequest, Long> {
    @Query(value="select * from plant_hire_request where UPPER(status) like %?1%", nativeQuery=true)
    List<PlantHireRequest> findPlantHireRequestsByStatus(String status);

    @Query(value="select p from PlantHireRequest p where p.purchaseOrder.poHref = ?1")
    List<PlantHireRequest> findPlantHireRequestByPOHref(String poHref);
}
