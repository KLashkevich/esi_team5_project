package com.buildit.procurement.domain.model;

import com.buildit.rental.domain.model.POStatus;
import lombok.*;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PRIVATE)
@AllArgsConstructor(staticName="of")
public class PurchaseOrder {
    String poHref;

    String poMessage;

    @Enumerated(EnumType.STRING)
    POStatus poStatus;

    public void setPoStatus(POStatus poStatus) {
        this.poStatus = poStatus;
    }

    public void setPoMessage(String poMessage) {
        this.poMessage = poMessage;
    }
}