package com.buildit.procurement.domain.model;

public enum PHRStatus {
    PENDING,
    REJECTED,
    ACCEPTED,
    CANCEL_REQUESTED,
    CANCELLED,
    PO_ACCEPTED,
    EXTENSION_REQUESTED,
    PLANT_DISPATCHED,
    PLANT_RETURNED,
    INVOICED,
    CLOSED
}