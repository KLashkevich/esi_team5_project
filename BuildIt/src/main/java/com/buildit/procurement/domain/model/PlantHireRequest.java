package com.buildit.procurement.domain.model;

import com.buildit.common.domain.BusinessPeriod;
import com.buildit.common.domain.ExtensionPeriod;
import com.buildit.invoicing.domain.PlantHireInvoice;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class PlantHireRequest {
    @Id
    @GeneratedValue
    Long id;

    String siteId;

    String comment;

    @Column(precision=8,scale=2)
    BigDecimal hiringCost;

    @Enumerated(EnumType.STRING)
    PHRStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

    @Embedded
    PurchaseOrder purchaseOrder;

    @Embedded
    ExtensionPeriod extensionPeriod;

    @Embedded
    PlantInventoryEntry inventoryEntry;

    @OneToOne
    PlantHireInvoice invoice;

    public static PlantHireRequest of(BusinessPeriod rentalPeriod, PlantInventoryEntry plantInventoryEntry) {
        PlantHireRequest phr = new PlantHireRequest();
        phr.rentalPeriod = rentalPeriod;
        phr.inventoryEntry = plantInventoryEntry;
        phr.status = PHRStatus.PENDING;
        return phr;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public void setHiringCost(BigDecimal hiringCost) {
        this.hiringCost = hiringCost;
    }

    public void setStatus(PHRStatus phrStatus) {
        this.status = phrStatus;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder){
        this.purchaseOrder = purchaseOrder;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setRentalPeriod (BusinessPeriod rentalPeriod) {
        this.rentalPeriod = rentalPeriod;
    }

    public void setExtensionPeriod(ExtensionPeriod extensionPeriod){
        this.extensionPeriod = extensionPeriod;
    }

    public void setPlantHireInvoice (PlantHireInvoice invoice) {
        this.invoice = invoice;
    }

}
