package com.buildit.procurement.application.service;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.application.dto.ExtensionPeriodDTO;
import com.buildit.common.rest.ExtendedLink;
import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.application.dto.PurchaseOrderDTO;
import com.buildit.procurement.domain.model.PlantHireRequest;
import com.buildit.procurement.rest.ProcurementRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.*;


@Service
public class PlantHireRequestAssembler extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestDTO> {

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public PlantHireRequestAssembler() {
        super(ProcurementRestController.class, PlantHireRequestDTO.class);
    }

    @Override
    public PlantHireRequestDTO toResource (PlantHireRequest phr) {
        PlantHireRequestDTO dto = createResourceWithId(phr.getId(), phr);
        dto.setSiteId(phr.getSiteId());
        dto.set_id(phr.getId());
        dto.setComment(phr.getComment());
        dto.setHiringCost(phr.getHiringCost());
        dto.setStatus(phr.getStatus());

        if(phr.getExtensionPeriod() != null){
            dto.setExtensionPeriodDTO(ExtensionPeriodDTO.of(phr.getExtensionPeriod().getExtensionStartDate(),
                    phr.getExtensionPeriod().getExtensionEndDate()));
        }

        dto.setRentalPeriod(BusinessPeriodDTO.of(phr.getRentalPeriod().getStartDate(), phr.getRentalPeriod().getEndDate()));

        if(phr.getPurchaseOrder() != null) {
            dto.setPurchaseOrder(PurchaseOrderDTO.of(phr.getPurchaseOrder().getPoHref(),
                    phr.getPurchaseOrder().getPoMessage(), phr.getPurchaseOrder().getPoStatus()));
        }
        dto.setInventoryEntry(plantInventoryEntryAssembler.toResource(phr.getInventoryEntry()));

        dto.add(new ExtendedLink(
                linkTo(methodOn(ProcurementRestController.class)
                        .findPlantHireRequestById(dto.get_id())).toString(),
                "fetch", GET));
        try {
            switch (phr.getStatus()) {
                case PENDING:
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(ProcurementRestController.class)
                                    .updatePlantHireRequest(dto.get_id(), new PlantHireRequestDTO())).toString(),
                            "update", PATCH));
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(ProcurementRestController.class)
                                    .acceptPlantHireRequest(dto.get_id())).toString(),
                            "accept", POST));
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(ProcurementRestController.class)
                                    .rejectPlantHireRequest(dto.get_id(), Optional.empty())).toString(),
                            "reject", DELETE));
                    break;
                default:
                    break;
            }
        } catch (Exception e) {}

        return dto;
    }
}
