package com.buildit.procurement.application.dto;

import com.buildit.rental.domain.model.POStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.Link;

import java.util.Map;

@Data
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
public class PurchaseOrderDTO {

    String poHref;
    String poMessage;
    POStatus status;

    @JsonProperty("_links")
    public void setLinks(final Map<String, Link> links) {
        poHref = links.get("self").getHref();
    }
}
