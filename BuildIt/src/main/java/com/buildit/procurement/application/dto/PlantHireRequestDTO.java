package com.buildit.procurement.application.dto;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.application.dto.ExtensionPeriodDTO;
import com.buildit.common.domain.ExtensionPeriod;
import com.buildit.common.rest.ResourceSupport;
import com.buildit.procurement.domain.model.PHRStatus;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PlantHireRequestDTO extends ResourceSupport {
    Long _id;
    String siteId;
    String comment;
    BigDecimal hiringCost;
    PHRStatus status;

    BusinessPeriodDTO rentalPeriod;
    ExtensionPeriodDTO extensionPeriodDTO;
    PurchaseOrderDTO purchaseOrder;
    PlantInventoryEntryDTO inventoryEntry;
}
