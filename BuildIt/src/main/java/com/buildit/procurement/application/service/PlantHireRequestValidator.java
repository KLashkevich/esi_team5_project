package com.buildit.procurement.application.service;

import com.buildit.common.application.service.BusinessPeriodValidator;
import com.buildit.procurement.domain.model.PlantHireRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class PlantHireRequestValidator implements Validator {

    @Autowired
    BusinessPeriodValidator businessPeriodValidator;

    @Override
    public boolean supports(Class<?> aClass) {
        return PlantHireRequest.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        //TODO: Implement validation logic here
    }
}
