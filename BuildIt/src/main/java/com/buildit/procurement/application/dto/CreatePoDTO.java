package com.buildit.procurement.application.dto;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor(staticName = "of")
public class CreatePoDTO {
    BusinessPeriodDTO rentalPeriod;
    PlantInventoryEntryDTO plant;
}
