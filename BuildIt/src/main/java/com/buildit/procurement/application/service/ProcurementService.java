package com.buildit.procurement.application.service;

import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.common.application.service.BusinessPeriodValidator;
import com.buildit.common.domain.BusinessPeriod;
import com.buildit.common.domain.ExtensionPeriod;
import com.buildit.procurement.application.dto.CreatePoDTO;
import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.application.dto.PurchaseOrderDTO;
import com.buildit.procurement.domain.model.PHRStatus;
import com.buildit.procurement.domain.model.PlantHireRequest;
import com.buildit.procurement.domain.model.PlantInventoryEntry;
import com.buildit.procurement.domain.model.PurchaseOrder;
import com.buildit.procurement.domain.repository.PlantHireRequestRepository;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.domain.model.POStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.web.client.RestTemplate;
import org.springframework.validation.Validator;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class ProcurementService {

    @Autowired
    PlantHireRequestRepository phrRepository;

    @Autowired
    PlantHireRequestAssembler phrAssembler;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    BusinessPeriodValidator bpValidator;

    @Autowired
    PlantHireRequestValidator phrValidator;

    @Autowired
    RestTemplate restTemplate;

    private final String rentItUrl = "http://localhost:8090/api/sales";

    public PlantHireRequestDTO findPlantHireRequest(Long id) throws Exception {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);

        if (phr == null) {
            throw new Exception(String.format("Plant Hire Request with id %s does not exist", id));
        }

        return phrAssembler.toResource(phr);
    }

    public List<PlantHireRequestDTO> findPlantHireRequests(String status) throws Exception {
        List <PlantHireRequest> requests;

        if(status != null && !status.isEmpty()) {
            if (isPlantHireRequestState(status)) {
                requests = phrRepository.findPlantHireRequestsByStatus(status.toUpperCase());
            }
            else {
                throw new Exception(String.format("Incorrect PHR Status '%s'", status));
            }
        }
        else {
            requests = phrRepository.findAll();
        }

        return phrAssembler.toResources(requests);
    }

    public PlantHireRequestDTO createPlantHireRequest(PlantHireRequestDTO phrDTO) throws Exception {
        BusinessPeriod period = BusinessPeriod.of(phrDTO.getRentalPeriod().getStartDate(), phrDTO.getRentalPeriod().getEndDate());

        DataBinder validationBinder = new DataBinder(period);
        validateBinder(validationBinder, bpValidator);

        PlantInventoryEntryDTO plantDTO = getPlantById(phrDTO.getInventoryEntry().get_id());

        PlantInventoryEntry plantInventoryEntry = PlantInventoryEntry.of(plantDTO.get_id().toString(),
                plantDTO.getName());

        PlantHireRequest phr = PlantHireRequest.of(period, plantInventoryEntry);
        phr.setSiteId(phrDTO.getSiteId());

        BigDecimal totalCost = new BigDecimal(0);
        if (plantDTO.getPrice() != null) {
            Long days = DAYS.between(period.getStartDate(), period.getEndDate());
            totalCost = plantDTO.getPrice().multiply(new BigDecimal(days));
        }

        phr.setHiringCost(totalCost);

        validationBinder = new DataBinder(phr);
        validateBinder(validationBinder, phrValidator);

        PlantHireRequest save = phrRepository.save(phr);
        return  phrAssembler.toResource(save);
    }

    public PlantHireRequestDTO updatePlantHireRequest(Long id, PlantHireRequestDTO patchDTO) throws Exception {
        PlantHireRequest phr = phrRepository.findById(id).orElse(null);
        if (phr == null) {
            throw new Exception(String.format("Plant hire request with id %s not found!", id));
        }

        if (phr.getStatus() != PHRStatus.PENDING) {
            throw new Exception("PHR STATUS is not PENDING, so it cannot be modified!");
        }

        BusinessPeriod period = BusinessPeriod.of(patchDTO.getRentalPeriod().getStartDate(), patchDTO.getRentalPeriod().getEndDate());

        DataBinder validationBinder = new DataBinder(period);
        validateBinder(validationBinder, bpValidator);

        // Hack to recalculate total cost without calling rentIt to receive entry price
        Long days = DAYS.between(phr.getRentalPeriod().getStartDate(), phr.getRentalPeriod().getEndDate());
        BigDecimal oneDayCost = phr.getHiringCost().divide(new BigDecimal(days));
        days = DAYS.between(period.getStartDate(), period.getEndDate());
        BigDecimal newTotalCost = oneDayCost.multiply(new BigDecimal(days));

        phr.setRentalPeriod(period);
        phr.setSiteId(patchDTO.getSiteId());
        phr.setHiringCost(newTotalCost);

        validationBinder = new DataBinder(phr);
        validateBinder(validationBinder, phrValidator);

        PlantHireRequest updatedPhr = phrRepository.save(phr);
        return  phrAssembler.toResource(updatedPhr);
    }

    public PlantHireRequestDTO acceptPlantHireRequest(Long id) throws Exception {
        PlantHireRequest plantHireRequest = phrRepository.findById(id).orElse(null);
        if (plantHireRequest == null) {
            throw new Exception("Plant Hire Request not found!");
        }

        if(plantHireRequest.getStatus() != PHRStatus.PENDING) {
            throw new Exception("PHR STATUS is not PENDING, so it cannot be accepted!");
        }

        DataBinder validationBinder = new DataBinder(plantHireRequest);
        validateBinder(validationBinder, phrValidator);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        BusinessPeriodDTO businessPeriodDTO = BusinessPeriodDTO.of(plantHireRequest.getRentalPeriod().getStartDate(), plantHireRequest.getRentalPeriod().getEndDate());
        PlantInventoryEntryDTO plantInventoryEntryDTO = plantInventoryEntryAssembler.toResource(plantHireRequest.getInventoryEntry());
        CreatePoDTO purchaseOrderDTO = CreatePoDTO.of(businessPeriodDTO, plantInventoryEntryDTO);
        HttpEntity entity = new HttpEntity<CreatePoDTO>(purchaseOrderDTO, headers);
        ResponseEntity<PurchaseOrderDTO> response =
                restTemplate.postForEntity(rentItUrl + "/orders", entity, PurchaseOrderDTO.class);
        if (response.getStatusCode() == HttpStatus.CREATED) {
            PurchaseOrderDTO responsePO = response.getBody();
            String poHref = responsePO.getPoHref().substring(responsePO.getPoHref().lastIndexOf('/') + 1);
            PurchaseOrder purchaseOrder = PurchaseOrder.of(poHref, responsePO.getPoMessage(), responsePO.getStatus());
            if(purchaseOrder.getPoStatus() == POStatus.REJECTED){
                plantHireRequest.setStatus(PHRStatus.REJECTED);
            }
            if(purchaseOrder.getPoStatus() == POStatus.PENDING){
                plantHireRequest.setStatus(PHRStatus.ACCEPTED);
            }
            plantHireRequest.setPurchaseOrder(purchaseOrder);
            validationBinder = new DataBinder(plantHireRequest);
            validateBinder(validationBinder, phrValidator);
            phrRepository.save(plantHireRequest);
        } else {
            throw new Exception("PO creation failed");
        }
        return phrAssembler.toResource(plantHireRequest);
    }

    public PlantHireRequestDTO cancelPlantHireRequest(Long id) throws Exception {
        PlantHireRequest plantHireRequest = phrRepository.findById(id).orElse(null);
        if (plantHireRequest == null) {
            throw new Exception(String.format("Plant hire request with id %s not found!", id));
        }

        LocalDate currentDate = LocalDate.now();
        LocalDate dispatchDate = plantHireRequest.getRentalPeriod().getStartDate();
        if (DAYS.between(currentDate, dispatchDate) <= 1) {
            throw new Exception("PHR cancellation is allowed until the day " +
                    "before the plant is due to be dispatched");
        }

        switch (plantHireRequest.getStatus()) {
            case PENDING:
                plantHireRequest.setStatus(PHRStatus.CANCELLED);
                break;
            case ACCEPTED:
                plantHireRequest.setStatus(PHRStatus.CANCELLED);
                if (plantHireRequest.getPurchaseOrder() != null) {
                    String poId = plantHireRequest.getPurchaseOrder().getPoHref();
                    String url = rentItUrl + "/orders/" + poId + "/cancel?requestNeeded=false";
                    ResponseEntity<PurchaseOrderDTO> response =
                            restTemplate.postForEntity(url, null,
                                    PurchaseOrderDTO.class);
                    if(response.getStatusCode() != HttpStatus.OK) {
                        throw new Exception("Something went wrong");
                    }
                }
                break;

            case PO_ACCEPTED:
                plantHireRequest.setStatus(PHRStatus.CANCEL_REQUESTED);
                if (plantHireRequest.getPurchaseOrder() != null) {
                    String poId = plantHireRequest.getPurchaseOrder().getPoHref();
                    String url = rentItUrl + "/orders/" + poId + "/cancel?requestNeeded=true";
                    ResponseEntity<PurchaseOrderDTO> response =
                            restTemplate.postForEntity(url, null,
                                    PurchaseOrderDTO.class);
                    if(response.getStatusCode() != HttpStatus.OK) {
                        throw new Exception("Something went wrong");
                    }
                }
                break;
            default:
                throw new Exception("PHR cannot be canceled, the status is incorrect!");
        }

        DataBinder validationBinder = new DataBinder(plantHireRequest);
        validateBinder(validationBinder, phrValidator);
        phrRepository.save(plantHireRequest);
        return phrAssembler.toResource(plantHireRequest);
    }

    public PlantHireRequestDTO rejectPlantHireRequest(Long id, String comment) throws Exception {
        PlantHireRequest plantHireRequest = phrRepository.findById(id).orElse(null);
        if(plantHireRequest == null ) {
            throw new Exception("PHR does not exist!");
        }

        if(plantHireRequest.getStatus() != PHRStatus.PENDING) {
            throw new Exception("PHR STATUS is not PENDING, so it cannot be rejected!");
        }

        plantHireRequest.setStatus(PHRStatus.REJECTED);
        if (comment != null) {
            plantHireRequest.setComment(comment);
        }

        DataBinder validationBinder = new DataBinder(plantHireRequest);
        validateBinder(validationBinder, phrValidator);
        phrRepository.save(plantHireRequest);
        return phrAssembler.toResource(plantHireRequest);
    }

    public PlantHireRequestDTO updatePurchaseOrderInfo(Long id, POStatus status, String comment) throws Exception{
        List<PlantHireRequest> plantHireRequests = phrRepository.findPlantHireRequestByPOHref(String.valueOf(id));
        PlantHireRequest plantHireRequest = null;
        if (plantHireRequests.size() == 1)
            plantHireRequest = plantHireRequests.get(0);

        if (plantHireRequests.size() > 1) {
            throw new Exception("Two or more PHRs have the same PO id");
        }

        if(plantHireRequest == null ) {
            throw new Exception("PHR with such a PO does not exist!");
        }

      //  if(plantHireRequest.getStatus() != PHRStatus.ACCEPTED) {
      //      throw new Exception("PHR STATUS is not ACCEPTED, so it has no PO to accept or reject!");
      //  }

        switch (status) {
            case OPEN:
                plantHireRequest.setStatus(PHRStatus.PO_ACCEPTED);
                plantHireRequest.getPurchaseOrder().setPoMessage(comment);
                plantHireRequest.getPurchaseOrder().setPoStatus(POStatus.OPEN);
                break;
            case PLANT_DISPATCHED:
                plantHireRequest.setStatus(PHRStatus.PLANT_DISPATCHED);
                plantHireRequest.getPurchaseOrder().setPoStatus(POStatus.PLANT_DISPATCHED);
                break;
            default:
                plantHireRequest.setStatus(PHRStatus.REJECTED);
                plantHireRequest.getPurchaseOrder().setPoMessage(comment);
                plantHireRequest.getPurchaseOrder().setPoStatus(POStatus.REJECTED);
        }
        DataBinder validationBinder = new DataBinder(plantHireRequest);
        validateBinder(validationBinder, phrValidator);
        phrRepository.save(plantHireRequest);
        return phrAssembler.toResource(plantHireRequest);
    }

    public PurchaseOrderDTO requestPoExtension(LocalDate newEndDate, Long oid) throws Exception {
        List<PlantHireRequest> plantHireRequests = phrRepository.findPlantHireRequestByPOHref(String.valueOf(oid));
        if(plantHireRequests.size() != 1){
            throw new Exception("PO PHR does not exist");
        }
        PlantHireRequest phr = plantHireRequests.get(0);
        if(phr.getRentalPeriod().getEndDate().isAfter(newEndDate)){
            throw new Exception("PO new end date cannot be before current end date");
        }
        if(phr.getStatus() == PHRStatus.PO_ACCEPTED && phr.getPurchaseOrder().getPoStatus() == POStatus.OPEN ) {
            String url = rentItUrl + "/orders/" + oid.toString() + "/extension?newEndDate=" + newEndDate.toString();
            ResponseEntity<PurchaseOrderDTO> response =
                    restTemplate.postForEntity(url, null,
                            PurchaseOrderDTO.class);
            if(response.getStatusCode() != HttpStatus.OK) {
                throw new Exception("Something went wrong");
            }
            phr.setExtensionPeriod(ExtensionPeriod.of(phr.getRentalPeriod().getStartDate(), newEndDate));
            phr.setStatus(PHRStatus.EXTENSION_REQUESTED);
            phr.getPurchaseOrder().setPoStatus(POStatus.PENDING_EXTENSION);
            phrRepository.save(phr);
            return response.getBody();
        }
        throw new Exception("PHR status is not Accepted or PO status is not OPEN");

    }

    public PlantHireRequestDTO updateExtension(Long poId, boolean extended) throws Exception {
        List<PlantHireRequest> plantHireRequests = phrRepository.findPlantHireRequestByPOHref(String.valueOf(poId));
        if(plantHireRequests.size() != 1){
            throw new Exception("PO PHR does not exist");
        }
        PlantHireRequest phr = plantHireRequests.get(0);
        if(extended){
            phr.setRentalPeriod(BusinessPeriod.of(phr.getExtensionPeriod().getExtensionStartDate(),
                    phr.getExtensionPeriod().getExtensionEndDate()));
        }
        phr.setExtensionPeriod(null);
        phr.setStatus(PHRStatus.PO_ACCEPTED);
        phr.getPurchaseOrder().setPoStatus(POStatus.OPEN);
        phrRepository.save(phr);
        return phrAssembler.toResource(phr);
    }

    public PlantHireRequestDTO acceptPOCancellation(Long poId) throws Exception {
        List<PlantHireRequest> plantHireRequests = phrRepository.findPlantHireRequestByPOHref(String.valueOf(poId));
        if(plantHireRequests.size() != 1){
            throw new Exception("PO PHR does not exist");
        }
        PlantHireRequest phr = plantHireRequests.get(0);

        phr.setStatus(PHRStatus.CANCELLED);
        phr.getPurchaseOrder().setPoStatus(POStatus.CANCELED);
        phrRepository.save(phr);
        return phrAssembler.toResource(phr);
    }

    private void validateBinder(DataBinder dataBinder, Validator validator) throws Exception {
        dataBinder.addValidators(validator);
        dataBinder.validate();
        if (dataBinder.getBindingResult().hasErrors()) {
            throw new Exception(dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode())
                    .collect(Collectors.toList()).toString());
        }
    }

    private boolean isPlantHireRequestState (String status) {
        for (PHRStatus s : PHRStatus.values()) {
            if (s.name().contains(status.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    private PlantInventoryEntryDTO getPlantById(Long id) throws Exception {
        ResponseEntity<PlantInventoryEntryDTO> response;
        PlantInventoryEntryDTO plantDTO;
        try {
            response = restTemplate.getForEntity(rentItUrl + "/plants/" + id, PlantInventoryEntryDTO.class);
            plantDTO = response.getBody();
        } catch (Exception e) {
            throw new Exception(String.format("Plant inventory entry with id %s does not exist", id));
        }
        return plantDTO;
    }
}
