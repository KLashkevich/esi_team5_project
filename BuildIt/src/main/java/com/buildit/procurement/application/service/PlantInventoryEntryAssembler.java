package com.buildit.procurement.application.service;

import com.buildit.procurement.domain.model.PlantInventoryEntry;
import com.buildit.procurement.rest.ProcurementRestController;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInventoryEntryAssembler extends ResourceAssemblerSupport<PlantInventoryEntry, PlantInventoryEntryDTO> {
    public PlantInventoryEntryAssembler() {
        super(ProcurementRestController.class, PlantInventoryEntryDTO.class);
    }

    @Override
    public PlantInventoryEntryDTO toResource(PlantInventoryEntry plant) {
        PlantInventoryEntryDTO dto = createResourceWithId(plant.getPlantHref(), plant);
        dto.set_id(getIdValue(plant.getPlantHref()));
        dto.setName(plant.getPlantName());

        return  dto;
    }

    private Long getIdValue (String href) {
        Long result;
        try {
            result = Long.parseLong(href);
        } catch (Exception ex) {
            result = 0L;
        }
        return result;
    }
}
