package com.buildit.procurement.rest;

import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.application.dto.PurchaseOrderDTO;
import com.buildit.procurement.application.service.ProcurementService;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.services.RentalService;
import com.buildit.rental.domain.model.POStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/procurements")
public class ProcurementRestController {

    @Autowired
    RentalService rentalService;

    @Autowired
    ProcurementService procurementService;

    private final String ROLE_ADMIN = "ROLE_ADMIN";
    private final String ROLE_WORKS = "ROLE_WORKS_ENGINEER";
    private final String ROLE_SITE = "ROLE_SITE_ENGINEER";

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/plants")
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name", required = false) Optional<String> plantName,
            @RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> startDate,
            @RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> endDate
        ){
        return rentalService.findAvailablePlants(getOptionalValue(plantName), getOptionalValue(startDate), getOptionalValue(endDate));
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/phrs")
    public ResponseEntity createPlantHireRequest(@RequestBody PlantHireRequestDTO partialDTO) {
        try {
            PlantHireRequestDTO newDTO = procurementService.createPlantHireRequest(partialDTO);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(newDTO.getId().getHref()));

            return new ResponseEntity<>(newDTO, headers, HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PatchMapping("/phrs/{id}")
    @Secured({ROLE_ADMIN, ROLE_WORKS, ROLE_SITE})
    public ResponseEntity updatePlantHireRequest(@PathVariable(value="id") Long id, @RequestBody PlantHireRequestDTO partialDTO) {
        try {
            PlantHireRequestDTO newDTO = procurementService.updatePlantHireRequest(id, partialDTO);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(newDTO.getId().getHref()));

            return new ResponseEntity<>(newDTO, headers, HttpStatus.OK);
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/phrs/{id}")
    public ResponseEntity findPlantHireRequestById(@PathVariable(value="id") Long id){
        try {
            PlantHireRequestDTO plantHireRequest = procurementService.findPlantHireRequest(id);
            return ResponseEntity.status(HttpStatus.OK)
                    .body(plantHireRequest);
        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/phrs")
    public ResponseEntity findPlantHireRequestByStatus(@RequestParam(value="status",required = false) String status) {
        try {
            List<PlantHireRequestDTO> plantHireRequestDTOS = procurementService.findPlantHireRequests(status);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(plantHireRequestDTOS);
        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: "+ e.getMessage());
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/phrs/{id}/accept")
    @Secured({ROLE_ADMIN, ROLE_WORKS})
    public ResponseEntity acceptPlantHireRequest(@PathVariable(value="id") Long id) {
        try {
            PlantHireRequestDTO plantHireRequestDTO = procurementService.acceptPlantHireRequest(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(plantHireRequestDTO);
        } catch (Exception e){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/phrs/{id}/cancel")
    @Secured({ROLE_ADMIN, ROLE_SITE})
    public ResponseEntity cancelPlantHireRequest(@PathVariable(value="id") Long id) {
        try {
            PlantHireRequestDTO plantHireRequestDTO = procurementService.cancelPlantHireRequest(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(plantHireRequestDTO);
        } catch (Exception e){
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/phrs/{id}/reject")
    @Secured({ROLE_ADMIN, ROLE_WORKS})
    public ResponseEntity rejectPlantHireRequest(@PathVariable(value="id") Long id, @RequestParam(name = "message", required = false) Optional<String> message) {
        try {
            String comment = getOptionalValue(message);
            PlantHireRequestDTO plantHireRequestDTO = procurementService.rejectPlantHireRequest(id, comment);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(plantHireRequestDTO);
        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @PostMapping("/orders/{oid}/updatepo")
    public ResponseEntity updatePurchaseOrderInfo(@PathVariable(value="oid") Long oid, @RequestParam String status, @RequestBody String comment){
        try {
            POStatus poStatus = POStatus.valueOf(status.toUpperCase());
            PlantHireRequestDTO plantHireRequestDTO = procurementService.updatePurchaseOrderInfo(oid, poStatus, comment);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(plantHireRequestDTO);
        } catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());
        }

    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/orders/{oid}/modify")
    public ResponseEntity requestExtendPO(@PathVariable(value = "oid") Long oid,
                                          @RequestParam(name = "newEndDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate newEndDate){
        try {
            PurchaseOrderDTO plantHireRequestDTO = procurementService.requestPoExtension(newEndDate, oid);
            return ResponseEntity.status(HttpStatus.OK).body(
                    plantHireRequestDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: " + e.getMessage());
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/orders/{oid}/updatePOExtension")
    public ResponseEntity updatePOExtension(@PathVariable(value="oid") Long poId, @RequestParam(value="extended") boolean extended){
        try {
            PlantHireRequestDTO plantHireRequestDTO = procurementService.updateExtension(poId, extended);
            return ResponseEntity.status(HttpStatus.OK).body(
                    plantHireRequestDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: " + e.getMessage());
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/orders/{oid}/approveCancel")
    public ResponseEntity acceptPOCancellation(@PathVariable(value="oid") Long poId){
        try {
            PlantHireRequestDTO plantHireRequestDTO = procurementService.acceptPOCancellation(poId);
            return ResponseEntity.status(HttpStatus.OK).body(
                    plantHireRequestDTO);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR: " + e.getMessage());
        }
    }


    private <T> T getOptionalValue (Optional<T> o) {
        if (o.isPresent()) {
            return o.get();
        } else {
            return null;
        }
    }
}
