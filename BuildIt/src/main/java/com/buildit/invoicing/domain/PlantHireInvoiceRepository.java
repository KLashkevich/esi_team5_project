package com.buildit.invoicing.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlantHireInvoiceRepository extends JpaRepository<PlantHireInvoice, Long> {
    @Query(value="select * from plant_hire_invoice where UPPER(status) like %?1%", nativeQuery = true)
    List<PlantHireInvoice> findPlantHireInvoicesByStatus(String status);

    @Query(value="select * from plant_hire_invoice where source_id = ?1", nativeQuery = true)
    List<PlantHireInvoice> findBySourceId(Long sourceId);
}