package com.buildit.invoicing.domain;

public enum InvoiceStatus {
    PENDING,
    REMINDER_SENT,
    PAID,
    REJECTED
}
