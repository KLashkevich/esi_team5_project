package com.buildit.invoicing.domain;

import com.buildit.procurement.domain.model.PHRStatus;
import com.buildit.procurement.domain.model.PlantHireRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class PlantHireInvoice {
    @Id
    @GeneratedValue
    Long id;

    Long sourceId; // Invoice ID in RentIt

    @OneToOne
    PlantHireRequest phr;

    @Column(precision=8,scale=2)
    BigDecimal totalCost;

    @Enumerated(EnumType.STRING)
    InvoiceStatus status;

    LocalDate dueDate;

    public static PlantHireInvoice of (PlantHireRequest phr, LocalDate dueDate) {
        PlantHireInvoice invoice = new PlantHireInvoice();
        invoice.dueDate = dueDate;
        invoice.phr = phr;
        invoice.status = InvoiceStatus.PENDING;
        return  invoice;
    }

    public void setStatus(InvoiceStatus status) {
        this.status = status;
    }

    public void setPlantHireRequest(PlantHireRequest phr) {
        this.phr = phr;
    }

    public void setSourceId (Long sourceId) {
        this.sourceId = sourceId;
    }

    public void setTotalCost (BigDecimal totalCost) {
        this.totalCost = totalCost;
    }
}
