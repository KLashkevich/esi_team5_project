package com.buildit.invoicing.application.service;

import com.buildit.invoicing.application.dto.InvoiceTransferDTO;
import com.buildit.invoicing.application.dto.PlantHireInvoiceDTO;
import com.buildit.invoicing.domain.InvoiceStatus;
import com.buildit.invoicing.domain.PlantHireInvoice;
import com.buildit.invoicing.domain.PlantHireInvoiceRepository;
import com.buildit.procurement.domain.model.PHRStatus;
import com.buildit.procurement.domain.model.PlantHireRequest;
import com.buildit.procurement.domain.repository.PlantHireRequestRepository;
import com.buildit.rental.domain.model.POStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class InvoicingService {

    @Autowired
    PlantHireInvoiceRepository invoiceRepository;

    @Autowired
    PlantHireInvoiceAssembler invoiceAssembler;

    @Autowired
    PlantHireInvoiceValidator invoiceValidator;

    @Autowired
    PlantHireRequestRepository phrRepository;

    @Autowired
    RestTemplate restTemplate;

    private String rentItInvoicingUrl = "http://localhost:8090/api/invoicing";

    public PlantHireInvoiceDTO findInvoice(Long id) throws Exception {
        PlantHireInvoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice == null) {
            throw new Exception(String.format("Plant hire invoice with id %s does not exist", id));
        }

        return invoiceAssembler.toResource(invoice);
    }

    public List<PlantHireInvoiceDTO> findInvoices(String status) throws Exception {
        List<PlantHireInvoice> invoices;

        if(status != null && !status.isEmpty()) {
            if (isInvoiceState(status)) {
                invoices = invoiceRepository.findPlantHireInvoicesByStatus(status.toUpperCase());
            } else {
                throw new Exception(String.format("Incorrect Status '%s'", status));
            }
        } else {
            invoices = invoiceRepository.findAll();
        }

        return invoiceAssembler.toResources(invoices);
    }

    public PlantHireInvoiceDTO createPlantHireInvoice (InvoiceTransferDTO dto) throws Exception {
        List<PlantHireRequest> plantHireRequests = phrRepository.findPlantHireRequestByPOHref(String.valueOf(dto.getPoId()));
        if(plantHireRequests.size() != 1){
            throw new Exception("PO PHR does not exist");
        }
        PlantHireRequest phr = plantHireRequests.get(0);

        PlantHireInvoice invoice = PlantHireInvoice.of(phr, dto.getDueDate());
        invoice.setStatus(dto.getStatus());
        invoice.setSourceId(dto.getSourceId());
        invoice.setTotalCost(dto.getTotalCost());

        validate(invoice, invoiceValidator);

        invoice = invoiceRepository.save(invoice);
        phr.setPlantHireInvoice(invoice);
        phr.setStatus(PHRStatus.INVOICED);
        phr.getPurchaseOrder().setPoStatus(POStatus.INVOICED);
        phrRepository.save(phr);

        return invoiceAssembler.toResource(invoice);
    }

    public PlantHireInvoiceDTO acceptPlantHireInvoice (Long id) throws Exception {
        PlantHireInvoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice == null) {
            throw new Exception(String.format("Plant hire invoice with id %s does not exist", id));
        }

        if (invoice.getStatus() != InvoiceStatus.PENDING && invoice.getStatus() != InvoiceStatus.REMINDER_SENT) {
            throw new Exception("Accepting payment is not allowed for current status");
        }

        invoice.setStatus(InvoiceStatus.PAID);

        ResponseEntity<PlantHireInvoiceDTO> response = restTemplate.postForEntity(
                rentItInvoicingUrl + "/invoices/" + invoice.getSourceId() + "/pay",
                null, PlantHireInvoiceDTO.class);

        if(response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("Could not pay on the supplier's end");
        }

        return invoiceAssembler.toResource(invoiceRepository.save(invoice));
    }

    public PlantHireInvoiceDTO rejectPlantHireInvoice (Long id) throws Exception {
        PlantHireInvoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice == null) {
            throw new Exception(String.format("Plant hire invoice with id %s does not exist", id));
        }

        if (invoice.getStatus() != InvoiceStatus.PENDING && invoice.getStatus() != InvoiceStatus.REMINDER_SENT) {
            throw new Exception("Rejecting payment is not allowed for current status");
        }

        invoice.setStatus(InvoiceStatus.REJECTED);

        ResponseEntity<PlantHireInvoiceDTO> response = restTemplate.exchange(
                rentItInvoicingUrl + "/invoices/" + invoice.getSourceId() + "/reject",
                HttpMethod.DELETE,
                null,
                PlantHireInvoiceDTO.class);

        if(response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("Could not reject invoice on the supplier's end");
        }

        return invoiceAssembler.toResource(invoiceRepository.save(invoice));
    }

    public PlantHireInvoiceDTO setReminder (Long id) throws Exception {
        List<PlantHireInvoice> invoices = invoiceRepository.findBySourceId(id);
        if (invoices.size() < 1) {
            throw new Exception(String.format("Plant hire invoice with source id %s does not exist", id));
        }

        PlantHireInvoice invoice = invoices.get(0);

        if (invoice.getStatus() != InvoiceStatus.PENDING) {
            throw new Exception("Sending reminders is allowed for pending invoices only");
        }

        // TODO: Add check that payment due date has passed

        invoice.setStatus(InvoiceStatus.REMINDER_SENT);

        return invoiceAssembler.toResource(invoiceRepository.save(invoice));
    }

    private boolean isInvoiceState (String status) {
        for (InvoiceStatus s : InvoiceStatus.values()) {
            if (s.name().contains(status.toUpperCase())) {
                return true;
            }
        }

        return false;
    }

    private void validate(Object o, Validator validator) throws Exception {
        DataBinder dataBinder = new DataBinder(o);
        dataBinder.addValidators(validator);
        dataBinder.validate();
        if (dataBinder.getBindingResult().hasErrors()) {
            throw new Exception(dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode())
                    .collect(Collectors.toList()).toString());
        }
    }
}
