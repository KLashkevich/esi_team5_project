package com.buildit.invoicing.application.dto;

import com.buildit.common.rest.ResourceSupport;
import com.buildit.invoicing.domain.InvoiceStatus;
import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class PlantHireInvoiceDTO extends ResourceSupport {
    Long _id;
    Long sourceId;
    PlantHireRequestDTO phr;
    BigDecimal totalCost;
    InvoiceStatus status;
    LocalDate dueDate;
}
