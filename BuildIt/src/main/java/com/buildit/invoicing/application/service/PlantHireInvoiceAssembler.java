package com.buildit.invoicing.application.service;

import com.buildit.invoicing.application.dto.PlantHireInvoiceDTO;
import com.buildit.invoicing.domain.PlantHireInvoice;
import com.buildit.invoicing.rest.InvoicingRestController;
import com.buildit.procurement.application.service.PlantHireRequestAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantHireInvoiceAssembler extends ResourceAssemblerSupport<PlantHireInvoice, PlantHireInvoiceDTO> {

    @Autowired
    PlantHireRequestAssembler phrAssembler;

    public PlantHireInvoiceAssembler() {
        super(InvoicingRestController.class, PlantHireInvoiceDTO.class);
    }

    @Override
    public PlantHireInvoiceDTO toResource(PlantHireInvoice invoice) {
        PlantHireInvoiceDTO dto = createResourceWithId(invoice.getId(), invoice);
        dto.set_id(invoice.getId());
        dto.setSourceId(invoice.getSourceId());
        dto.setTotalCost(invoice.getTotalCost());
        dto.setDueDate(invoice.getDueDate());
        dto.setStatus(invoice.getStatus());

        dto.setPhr(phrAssembler.toResource(invoice.getPhr()));

        // TODO: Add links

        return dto;
    }
}
