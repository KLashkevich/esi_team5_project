package com.buildit.invoicing.rest;

import com.buildit.invoicing.application.dto.InvoiceTransferDTO;
import com.buildit.invoicing.application.dto.PlantHireInvoiceDTO;
import com.buildit.invoicing.application.service.InvoicingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/invoicing")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class InvoicingRestController {

    @Autowired
    InvoicingService invoicingService;

    private final String ROLE_ADMIN = "ROLE_ADMIN";
    private final String ROLE_SITE = "ROLE_SITE_ENGINEER";

    @GetMapping("/invoices")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity findInvoices(@RequestParam(name = "status", required = false) String status) {
        try {
            List<PlantHireInvoiceDTO> invoices = invoicingService.findInvoices(status);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoices);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());

        }
    }

    @GetMapping("/invoices/{id}")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity findInvoiceById(@PathVariable("id") Long id) {
        try {
            PlantHireInvoiceDTO invoice = invoicingService.findInvoice(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoice);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @PostMapping("/invoices")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @Secured({ROLE_ADMIN})
    public ResponseEntity createInvoice(@RequestBody InvoiceTransferDTO transferDTO) {
        try {
            PlantHireInvoiceDTO newDTO = invoicingService.createPlantHireInvoice(transferDTO);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(newDTO.getId().getHref()));

            return new ResponseEntity<>(newDTO, headers, HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("/invoices/{id}/accept")
    @Secured({ROLE_ADMIN, ROLE_SITE})
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity acceptPlantHireInvoice(@PathVariable("id") Long id) {
        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoicingService.acceptPlantHireInvoice(id));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @DeleteMapping("/invoices/{id}/reject")
    @Secured({ROLE_ADMIN, ROLE_SITE})
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity rejectPlantHireInvoice(@PathVariable("id") Long id) {
        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoicingService.rejectPlantHireInvoice(id));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("/invoices/{id}/setReminder")
    @Secured({ROLE_ADMIN})
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity setReminder(@PathVariable("id") Long id) {
        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoicingService.setReminder(id));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }
}
