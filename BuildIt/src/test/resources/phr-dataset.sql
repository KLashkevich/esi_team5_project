INSERT INTO PLANT_HIRE_REQUEST
(id, site_id, comment, hiring_cost, status, start_date, end_date, po_href, po_status, po_message, plant_href, plant_name)
VALUES (1, '111', '', 300, 'PENDING', '2019-08-01', '2019-08-02', NULL, NULL, NULL, '1', '1.5 Tonne Mini excavator');

INSERT INTO PLANT_HIRE_REQUEST
(id, site_id, comment, hiring_cost, status, start_date, end_date, po_href, po_status, po_message, plant_href, plant_name)
VALUES (2, '111', 'Too expensive', 600, 'PENDING', '2019-08-05', '2019-08-07', NULL, NULL, NULL, '2', '3 Tonne Mini excavator');

INSERT INTO PLANT_HIRE_REQUEST
(id, site_id, comment, hiring_cost, status, start_date, end_date, po_href, po_status, po_message, plant_href, plant_name)
VALUES (3, '111', '', 400, 'PENDING', '2019-08-01', '2019-08-02', NULL, 'OPEN', '', '9', '2 Tonne Front Tip Dumper');

INSERT INTO PLANT_HIRE_REQUEST
(id, site_id, comment, hiring_cost, status, start_date, end_date, po_href, po_status, po_message, plant_href, plant_name)
VALUES (4, '111', '', 900, 'PENDING', '2019-08-01', '2019-08-02', NULL, 'REJECTED', 'Plant just broke', '1', '1.5 Tonne Mini excavator');

INSERT INTO PLANT_HIRE_REQUEST
(id, site_id, comment, hiring_cost, status, start_date, end_date, po_href, po_status, po_message, plant_href, plant_name)
VALUES (5, '222', '', 300, 'PENDING', '2019-09-01', '2019-09-02', NULL, NULL, NULL, '1', '1.5 Tonne Mini excavator');

INSERT INTO PLANT_HIRE_REQUEST
(id, site_id, comment, hiring_cost, status, start_date, end_date, po_href, po_status, po_message, plant_href, plant_name)
VALUES (6, '222', 'Too expensive', 600, 'PENDING', '2019-09-05', '2019-09-07', NULL, NULL, NULL, '2', '3 Tonne Mini excavator');

INSERT INTO PLANT_HIRE_REQUEST
(id, site_id, comment, hiring_cost, status, start_date, end_date, po_href, po_status, po_message, plant_href, plant_name)
VALUES (7, '222', '', 400, 'PENDING', '2019-09-01', '2019-09-02', NULL, 'OPEN', '', '9', '2 Tonne Front Tip Dumper');

INSERT INTO PLANT_HIRE_REQUEST
(id, site_id, comment, hiring_cost, status, start_date, end_date, po_href, po_status, po_message, plant_href, plant_name)
VALUES (8, '222', '', 900, 'PENDING', '2019-09-01', '2019-09-02', NULL, 'REJECTED', 'Plant just broke', '1', '1.5 Tonne Mini excavator');
