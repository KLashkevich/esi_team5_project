Feature: Rejected created PHR
  As a BuildIt employee
  So that I can decide to not allow the ordering of a plant
  I want to be able to reject a plant hire request

  # Covers CC5 (rejecting PHR)

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Rejecting a created plant hire request
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    When the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2020-01-22" to "2020-01-24"
    And creates a new plant hire request from one of the plants
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And clicks on the "rejectButton" button
    And "abcd" is left as the "rejectMessage"
    Then the state of the plant hire request should change to "REJECTED"
