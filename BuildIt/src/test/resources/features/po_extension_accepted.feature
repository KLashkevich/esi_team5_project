Feature: Accept PO extension
  As a BuildIt employee
  So that I can hire a plant for longer
  I want to be able to request a purchase order extension for an accepted purchase order

  # Covers PS6, CC8

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Asking for PO extension
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2019-12-23" to "2019-12-24"
    And creates a new plant hire request from one of the plants
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And the works engineer accepts the created plant hire request
    And on RentIt's site
    And the "acceptPO" button is pressed next to the created PO
    And "abcd" is left as the "acceptMessage"
    And a customer is in the "BuildIt" web page
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And clicks on the "nameButton" button
    And clicks on the "extendButton" button
    And enters the end date "2019-12-28"
    And clicks on the "extend" button
    Then the status of the plant hire request should change to "EXTENSION_REQUESTED" on BuildIt's side
    And the status of the PO should change to "PENDING_EXTENSION" on RentIt's side
    And two new actions should appear

  Scenario: Accepting PO extension
    When on RentIt's site
    And the "acceptExtension" button is pressed next to the created PO
    Then the status of the purchase order should change to "OPEN"
    And the end date should change to "2019-12-28"
    And the status of the plant hire request should change to "PO_ACCEPTED" on BuildIt's side
    And the extension end date should be "2019-12-28"
