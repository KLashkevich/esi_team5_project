Feature: Create and accept PHR and reject PO
  As a RentIt employee
  So that I can not allow a plant hire request
  I want to be able to see and reject a purchase order created by a site engineer

  # Covers CC6 (rejection), PS5

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Rejected a purchase order
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2019-10-22" to "2019-10-24"
    And creates a new plant hire request from one of the plants
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And the works engineer accepts the created plant hire request
    And on RentIt's site
    And the "rejectPO" button is pressed next to the created PO
    And "abcd" is left as the "rejectMessage"
    Then the status of the purchase order should change to "REJECTED"
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the status of the plant hire request should change to "REJECTED" on BuildIt's side
    And the status "PO status:REJECTED" and message "PO message: abcd" of the PO associated with the PHR should be visible
