Feature: Invoicing
  As a RentIt employee
  So that I can have a successful business
  I want to be able to send invoices to my customers

  # Covers CC9, CC10, CC11, CC12?, PS5, PS8, PS9, PS10, PS11 (manual button click), PS12

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Dispatching the plant
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2019-11-23" to "2019-11-24"
    And creates a new plant hire request from one of the plants
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And the works engineer accepts the created plant hire request
    And on RentIt's site
    And the "acceptPO" button is pressed next to the created PO
    And "abcd" is left as the "acceptMessage"
    And on RentIt's site
    And the "dispatchButton" button is pressed next to the created PO
    Then the status of the purchase order should change to "PLANT_DISPATCHED"
    And the status of the plant hire request should change to "PLANT_DISPATCHED" on BuildIt's side
    And the status "PO status:PLANT_DISPATCHED" and message "PO message: abcd" of the PO associated with the PHR should be visible

  Scenario: Returning the plant on RentIt
    When on RentIt's site
    And the "return" button is pressed next to the created PO
    Then the status of the purchase order should change to "INVOICED"
    And a "invoiceButton" button should become visible on RentIt
    And the invoice status should be "Status: PENDING"
    And the status of the plant hire request should change to "INVOICED" on BuildIt's side
    And on BuildIt's site invoice status should be "Status: PENDING"
    And the status "PO status:INVOICED" and message "PO message: abcd" of the PO associated with the PHR should be visible
    And the id of the PO associated with the PHR should be correct

  Scenario: Sending a reminder
    When on RentIt's site
    And the "invoiceButton" button is pressed next to the created PO
    And clicks on the "reminder" button
    Then the invoice status should be "Status: REMINDER_SENT"
    And on BuildIt's site invoice status should be "Status: REMINDER_SENT"

  Scenario: Paying for the plant
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And clicks on the "invoiceButton" button
    And clicks on the "pay" button
    Then on BuildIt's site invoice status should be "Status: PAID"
    And on RentIt's site
    And the invoice status should be "Status: PAID"
