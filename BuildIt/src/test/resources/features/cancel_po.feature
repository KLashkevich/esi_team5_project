Feature: Cancel PO success
  As a BuildIt employee
  So that I can decide to not order a plant
  I want to be able to cancel a created plant hire request and PO

  # Covers CC3, PS7

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Requesting to cancel a created purchase order
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2019-09-22" to "2019-09-24"
    And creates a new plant hire request from one of the plants
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And the works engineer accepts the created plant hire request
    And on RentIt's site
    And the "acceptPO" button is pressed next to the created PO
    And "abcd" is left as the "acceptMessage"
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And clicks on the "cancelButton" button
    Then the status of the plant hire request should change to "CANCEL_REQUESTED" on BuildIt's side

  Scenario: Accepting cancellation on RentIt's side
    When on RentIt's site
    And the "approveCancel" button is pressed next to the created PO
    Then the status of the purchase order should change to "CANCELED"
    And the status of the plant hire request should change to "CANCELLED" on BuildIt's side

