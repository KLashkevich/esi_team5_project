Feature: Extend pending PO
  As a BuildIt employee
  So that I can hire a plant for longer
  I want to be able to request a purchase order extension for a pending purchase order

  # Covers PS6, CC8

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Asking for invalid PO extension
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2020-09-23" to "2020-09-24"
    And creates a new plant hire request from one of the plants
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And the works engineer accepts the created plant hire request
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And clicks on the "nameButton" button
    And clicks on the "extendButton" button
    And enters the end date "2020-05-28"
    And clicks on the "extend" button
    Then there should be a message "ERROR: PO new end date cannot be before current end date"

  Scenario: Asking for PO extension
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And clicks on the "nameButton" button
    And clicks on the "extendButton" button
    And enters the end date "2020-09-28"
    And clicks on the "extend" button
    Then there should be a message "ERROR: PHR status is not Accepted or PO status is not OPEN"
