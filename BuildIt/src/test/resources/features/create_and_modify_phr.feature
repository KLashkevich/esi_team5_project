Feature: Modify created PHR
  As a BuildIt employee
  So that I can modify my plant hire request
  I want to be able to change the site id and the start and end date of my plant hire request

  # Covers CC1 and CC2, PS1, PS2, PS3

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Logging in
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    Then the user is logged in

  Scenario: Querying the plant catalog for an excavator
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2019-07-22" to "2019-07-24"
    Then 3 plants are shown

  Scenario: Selecting a plant to create plant hire request
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2019-07-22" to "2019-07-24"
    And creates a new plant hire request from one of the plants
    Then a new plant hire request is created

  Scenario: Modifying the created plant hire request
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And clicks on the "modifyButton" button
    And enters "5" as site id, "2019-07-25" as start date and "2019-07-27" as end date
    Then the PHR is modified
