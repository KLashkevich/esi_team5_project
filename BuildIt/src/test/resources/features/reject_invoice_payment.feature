Feature: Reject invoicing payment
  As a BuildIt employee
  So that I can decide to pay or not to pay for a purchase order
  I want to be able to reject invoice payment

  # CC11 with disapproval, CC12 with disapproval?

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Reject paying for a plant
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2020-05-23" to "2020-05-24"
    And creates a new plant hire request from one of the plants
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And the works engineer accepts the created plant hire request
    And on RentIt's site
    And the "acceptPO" button is pressed next to the created PO
    And "abcd" is left as the "acceptMessage"
    And on RentIt's site
    And the "dispatchButton" button is pressed next to the created PO
    And on RentIt's site
    And the "return" button is pressed next to the created PO
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And clicks on the "invoiceButton" button
    And clicks on the "rejectPay" button
    Then on BuildIt's site invoice status should be "Status: REJECTED"
    And on RentIt's site
    And the invoice status should be "Status: REJECTED"
