Feature: Create and accept PHR and PO
  As a BuildIt employee
  So that I can order a plant
  I want to be able to create a purchase order which can be accepted by RentIt

  # Covers CC4, CC5 (modifying and accepting PHR), CC6 (producing and forwarding a PO to supplier, accepting the PO) and CC7, PS4, PS5

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Accepting a created and modified plant hire request
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2019-08-22" to "2019-08-24"
    And creates a new plant hire request from one of the plants
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And clicks on the "modifyButton" button
    And enters "6" as site id, "2019-08-23" as start date and "2019-08-24" as end date
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And the works engineer accepts the created plant hire request
    Then the status of the plant hire request should change to "ACCEPTED" on BuildIt's side
    And a new purchase order is created on RentIt's side

  Scenario: Accepting the created purchase order in RentIt's side
    When on RentIt's site
    And the "acceptPO" button is pressed next to the created PO
    And "abcd" is left as the "acceptMessage"
    Then the status of the purchase order should change to "OPEN"
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the status of the plant hire request should change to "PO_ACCEPTED" on BuildIt's side
    And the status "PO status:OPEN" and message "PO message: abcd" of the PO associated with the PHR should be visible
