Feature: Cancel PHR not allowed
  As a BuildIt employee
  So that I don't need to deal with any problems
  I want to be able to cancel a plant only if it is allowed

  # Covers CC3 (Cancellations after they are allowed)

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button

  Scenario: Creating an uncancellable plant hire request
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "tomorrow" to "5 days from now"
    And creates a new plant hire request from one of the plants
    Then a new plant hire request is created
    And there should be no cancel button next to the PHR
