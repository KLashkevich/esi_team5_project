Feature: Reject PO extension
  As a RentIt employee
  So that I can limit a purchase order extension period
  I want to be able to reject a request to extend an accepted purchase order

  # Covers PS6, CC8

  Background: Plant catalog
    Given the following plants are available in the catalog
      | id | name           | description                      | price  |
      |  1 | Mini Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      |  2 | Mini Excavator | 3 Tonne Mini excavator           | 200.00 |
      |  3 | Midi Excavator | 5 Tonne Midi excavator           | 250.00 |
    And a customer is in the "BuildIt" web page

  Scenario: Rejecting PO extension
    When the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And the customer navigates to the "Create Plant Hire Request" tab
    And the customer queries the plant catalog for an "excavator" available from "2020-08-23" to "2020-08-24"
    And creates a new plant hire request from one of the plants
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "works" and password "works"
    And clicks on the "loginButton" button
    And the works engineer accepts the created plant hire request
    And on RentIt's site
    And the "acceptPO" button is pressed next to the created PO
    And "abcd" is left as the "acceptMessage"
    And a customer is in the "BuildIt" web page
    And the engineer enters the username "site" and password "site"
    And clicks on the "loginButton" button
    And clicks on the "nameButton" button
    And clicks on the "extendButton" button
    And enters the end date "2020-08-28"
    And clicks on the "extend" button
    And on RentIt's site
    And the "rejectExtension" button is pressed next to the created PO
    Then there should be a message "PO extension rejected!"
    And on BuildIt's side the PHR end date should still be "2020-08-24"
