package com.buildit.cucumber;

import com.buildit.BuilditApplication;
import com.buildit.procurement.domain.model.PlantInventoryEntry;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
@ContextConfiguration(classes = BuilditApplication.class)
@WebAppConfiguration
public class PlantCatalogSteps extends CucumberTest{


    private WebDriver driver;

    private List<PlantInventoryEntry> entries = new ArrayList<>();

    private static List<String> createdPlant = new ArrayList<>();
    private static String invoicedPOid;

    static {
        System.setProperty("webdriver.chrome.driver", "/path/chromedriver"); //specify chrome driver path
    }


    @Before
    public void setup() {
        driver = new ChromeDriver();
    }

    @After
    public void tearoff() {
    }

    @Given("^the following plants are available in the catalog$")
    public void the_following_plants_are_available_in_the_catalog(DataTable table) throws Throwable {
        for (Map<String, String> row: table.asMaps(String.class, String.class)){
            entries.add(PlantInventoryEntry.of(row.get("id"), row.get("name")));
        }
    }

    @And("^a customer is in the \"([^\"]*)\" web page$")
    public void a_customer_is_in_the_web_page(String arg1) throws Throwable {
        driver.get("http://localhost:4000/");
        driver.manage().window().maximize();
        ((JavascriptExecutor) driver).executeScript("window.focus();");
    }

    @When("^the engineer enters the username \"([^\"]*)\" and password \"([^\"]*)\"$")
    public void the_site_engineer_enters_the_username_and_password(String arg1, String arg2) throws Throwable {
        driver.findElement(By.id("username")).sendKeys(arg1);
        driver.findElement(By.id("password")).sendKeys(arg2);
    }

    @When("^clicks on the login button$")
    public void clicks_on_the_login_button() throws Throwable {
        driver.findElement(By.id("loginButton")).click();
    }

    @Then("^the user is logged in$")
    public void the_user_is_logged_in() throws Throwable {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(By.className("has-text-center"));
        assertThat(elements.get(0).getText()).isEqualTo("Id");
        assertThat(elements.get(1).getText()).isEqualTo("Plant Name");
        assertThat(elements.get(2).getText()).isEqualTo("Site ID");
        assertThat(elements.get(3).getText()).isEqualTo("Status");
        assertThat(elements.get(4).getText()).isEqualTo("Comment");
        assertThat(elements.get(5).getText()).isEqualTo("Start date");
        assertThat(elements.get(6).getText()).isEqualTo("End date");
        assertThat(elements.get(7).getText()).isEqualTo("Extension End Date");
        assertThat(elements.get(8).getText()).isEqualTo("Cost");
    }

    @When("^the customer navigates to the \"([^\"]*)\" tab$")
    public void the_customer_navigates_to_the_tab(String arg1) throws Throwable {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.className("home")).findElement(By.tagName("div")).findElements(By.tagName("div")).get(1)
                .findElement(By.tagName("nav")).findElement(By.tagName("ul")).findElements(By.tagName("li")).get(1).click();

        List<WebElement> elements = driver.findElements(By.id("plantId"));
        assertThat(elements.size()).isEqualTo(3);
    }

    @And("^the customer queries the plant catalog for an \"([^\"]*)\" available from \"([^\"]*)\" to \"([^\"]*)\"$")
    public void the_customer_queries_the_plant_catalog_for_an_available_from_to(String arg1, String arg2, String arg3) throws Throwable {
        createdPlant = new ArrayList<>();
        createdPlant.add(arg2);
        createdPlant.add(arg3);
        driver.findElements(By.xpath("//input")).get(0).sendKeys(arg1);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        if (arg2.equals("tomorrow")) {
            LocalDate d = LocalDate.now().plusDays(1);
            LocalDate later = LocalDate.now().plusDays(5);
            int year = d.getYear();
            int month = d.getMonthValue();
            int day = d.getDayOfMonth();
            int yearLater = later.getYear();
            int monthLater = later.getMonthValue();
            int dayLater = later.getDayOfMonth();
            js.executeScript("document.querySelector('.datepicker').__vue__.$data.dateSelected = new Date(" + year + "," + (month-1) + "," + day + ")");
            js.executeScript("document.querySelectorAll('.datepicker')[1].__vue__.$data.dateSelected = new Date(" + yearLater + "," + (monthLater-1) + "," + dayLater + ")");
        }
        else {
            String[] startDate = arg2.split("-");
            String[] endDate = arg3.split("-");
            js.executeScript("document.querySelector('.datepicker').__vue__.$data.dateSelected = new Date(" + startDate[0] + "," + (Integer.valueOf(startDate[1]) - 1) + "," + startDate[2] + ")");
            js.executeScript("document.querySelectorAll('.datepicker')[1].__vue__.$data.dateSelected = new Date(" + endDate[0] + "," + (Integer.valueOf(endDate[1]) - 1) + "," + endDate[2] + ")");
        }
        driver.findElement(By.id("submitButton")).click();
        Thread.sleep(2000);
    }

    @Then("^(\\d+) plants are shown$")
    public void plants_are_shown(int arg1) throws Throwable {
        List<WebElement> elements = driver.findElements(By.id("plantName"));
        assertThat(elements.size()).isEqualTo(arg1);
        assertThat(elements.get(0).getText()).isEqualTo("Mini excavator");
        assertThat(elements.get(1).getText()).isEqualTo("Midi excavator");
        assertThat(elements.get(2).getText()).isEqualTo("Mini excavator");
    }

    @And("^creates a new plant hire request from one of the plants$")
    public void creates_a_new_plant_hire_request_from_one_of_the_plants() throws Throwable {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        List<String> entry = new ArrayList<>();
        String plantId = driver.findElements(By.id("plantId")).get(0).getText();
        String plantDescription = driver.findElements(By.id("plantDescription")).get(0).getText();
        String plantName = driver.findElements(By.id("plantName")).get(0).getText();
        String plantPrice = driver.findElements(By.id("plantPrice")).get(0).getText();
        createdPlant.add(plantId);
        createdPlant.add(plantDescription);
        createdPlant.add(plantName);
        createdPlant.add(plantPrice);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.findElements(By.id("createPHR")).get(0).click();
    }

    @Then("^a new plant hire request is created$")
    public void a_new_plant_hire_request_is_created() throws Throwable {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        assertThat(driver.findElements(By.xpath("//*[contains(text(), 'Plant hire request has been successfully created!')]")).size()).isEqualTo(1);
    }

    @Then("^there should be no cancel button next to the PHR$")
    public void there_should_be_no_cancel_button_next_to_the_PHR() throws Throwable {
        driver.get("http://localhost:4000/");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.id("username")).sendKeys("site");
        driver.findElement(By.id("password")).sendKeys("site");
        driver.findElement(By.id("loginButton")).click();
        List<WebElement> elements = driver.findElements(By.id("buttons"));
        WebElement elem = elements.get(elements.size()-1);
        assertThat(elem.findElements(By.tagName("a")).size()).isEqualTo(1);
    }

    @Then("^the PHR is canceled$")
    public void the_PHR_is_canceled() throws Throwable {
        driver.get("http://localhost:4000/");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.id("username")).sendKeys("site");
        driver.findElement(By.id("password")).sendKeys("site");
        driver.findElement(By.id("loginButton")).click();
        List<WebElement> elements = driver.findElements(By.id("status"));
        assertThat(elements.get(elements.size()-1).getText()).isEqualTo("CANCELLED");
    }

    @When("^enters \"([^\"]*)\" as site id, \"([^\"]*)\" as start date and \"([^\"]*)\" as end date$")
    public void enters_as_site_id_as_start_date_and_as_end_date(String arg1, String arg2, String arg3) throws Throwable {
        driver.findElement(By.id("siteId")).sendKeys(arg1);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        createdPlant.remove(0);
        createdPlant.remove(0);
        createdPlant.add(0, arg3);
        createdPlant.add(0, arg2);
        String[] startDate = arg2.split("-");
        String[] endDate = arg3.split("-");
        js.executeScript("document.querySelector('.datepicker').__vue__.$data.dateSelected = new Date(" + startDate[0] + "," + (Integer.valueOf(startDate[1])-1) + "," + startDate[2] + ")");
        js.executeScript("document.querySelectorAll('.datepicker')[1].__vue__.$data.dateSelected = new Date(" + endDate[0] + "," + (Integer.valueOf(endDate[1])-1) + "," + endDate[2] + ")");
        driver.findElement(By.id("modify")).click();
    }

    @Then("^the PHR is modified with new data$")
    public void the_PHR_is_modified_with_new_data() throws Throwable {
        driver.get("http://localhost:4000/");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.id("username")).sendKeys("works");
        driver.findElement(By.id("password")).sendKeys("works");
        driver.findElement(By.id("loginButton")).click();
        List<WebElement> elements = driver.findElements(By.id("siteIdentification"));
        assertThat(driver.findElements(By.id("siteIdentification")).get(elements.size()-1).getText()).isEqualTo("6");
        assertThat(driver.findElements(By.id("start")).get(elements.size()-1).getText()).isEqualTo("2019-08-23");
        assertThat(driver.findElements(By.id("end")).get(elements.size()-1).getText()).isEqualTo("2019-08-24");
    }

    @Then("^the PHR is modified$")
    public void the_PHR_is_modified() throws Throwable {
        driver.get("http://localhost:4000/");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.id("username")).sendKeys("site");
        driver.findElement(By.id("password")).sendKeys("site");
        driver.findElement(By.id("loginButton")).click();
        List<WebElement> elements = driver.findElements(By.id("siteIdentification"));
        assertThat(driver.findElements(By.id("siteIdentification")).get(elements.size()-1).getText()).isEqualTo("5");
        assertThat(driver.findElements(By.id("start")).get(elements.size()-1).getText()).isEqualTo("2019-07-25");
        assertThat(driver.findElements(By.id("end")).get(elements.size()-1).getText()).isEqualTo("2019-07-27");
    }

    @When("^the works engineer accepts the created plant hire request$")
    public void accepts_the_created_plant_hire_request() throws Throwable {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        List<WebElement> statuses = driver.findElements(By.id("status"));
        assertThat(statuses.get(statuses.size()-1).getText()).isEqualTo("PENDING");
        List<WebElement> elements = driver.findElements(By.id("acceptButton"));
        elements.get(elements.size()-1).click();
    }

    @Then("^a new purchase order is created on RentIt's side$")
    public void a_new_purchase_order_is_created_on_RentIt_s_side() throws Throwable {
        driver.get("http://localhost:4010/");
        int count = driver.findElements(By.id("poId")).size();
        assertThat(driver.findElements(By.id("poStatus")).get(count-1).getText()).isEqualTo("PENDING");
        assertThat(driver.findElements(By.id("poDesc")).get(count-1).getText()).isEqualTo(createdPlant.get(3));
        assertThat(driver.findElements(By.id("poName")).get(count-1).getText()).isEqualTo(createdPlant.get(4));
        assertThat(driver.findElements(By.id("poPrice")).get(count-1).getText()).isEqualTo(createdPlant.get(5));
        assertThat(driver.findElements(By.id("poPlant")).get(count-1).getText()).isEqualTo(createdPlant.get(2));
        assertThat(driver.findElements(By.id("poStart")).get(count-1).getText()).isEqualTo(createdPlant.get(0));
        assertThat(driver.findElements(By.id("poEnd")).get(count-1).getText()).isEqualTo(createdPlant.get(1));
    }

    @When("^on RentIt's site$")
    public void on_RentIt_s_site() throws Throwable {
        driver.get("http://localhost:4010/");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Then("^the status of the purchase order should change to \"([^\"]*)\"$")
    public void the_status_of_the_purchase_order_should_change_to(String arg1) throws Throwable {
        driver.get("http://localhost:4000/"); // this part is only used as a delay to get correct outputs as just adding implicitWait doesn't seem to work
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.id("username")).sendKeys("works");
        driver.findElement(By.id("password")).sendKeys("works");
        driver.findElement(By.id("loginButton")).click();
        driver.get("http://localhost:4010/");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        int count = driver.findElements(By.id("poStatus")).size();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        assertThat(driver.findElements(By.id("poStatus")).get(count-1).getText()).isEqualTo(arg1);
    }

    @And("^the status of the plant hire request should change to \"([^\"]*)\" on BuildIt's side$")
    public void the_status_of_the_plant_hire_request_should_change_to_on_BuildIt_s_side(String arg1) throws Throwable {
        driver.get("http://localhost:4000/"); // this part is only used as a delay to get correct outputs as just adding implicitWait doesn't seem to work
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.id("username")).sendKeys("site");
        driver.findElement(By.id("password")).sendKeys("site");
        driver.findElement(By.id("loginButton")).click();
        List<WebElement> elems = driver.findElements(By.id("status"));
        assertThat(elems.get(elems.size()-1).getText()).isEqualTo(arg1);
    }

    @Then("^the state of the plant hire request should change to \"([^\"]*)\"$")
    public void then_the_state_of_the_plant_hire_request_should_change_to(String arg1) throws Throwable {
        driver.get("http://localhost:4000/");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.findElement(By.id("username")).sendKeys("works");
        driver.findElement(By.id("password")).sendKeys("works");
        driver.findElement(By.id("loginButton")).click();
        List<WebElement> statuses = driver.findElements(By.id("status"));
        assertThat(statuses.get(statuses.size()-1).getText()).isEqualTo(arg1);
        assertThat(driver.findElements(By.id("comment")).get(statuses.size()-1).getText()).isEqualTo("abcd");
    }

    @When("^enters the end date \"([^\"]*)\"$")
    public void enters_the_end_date(String arg1) throws Throwable {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String[] endDate = arg1.split("-");
        js.executeScript("document.querySelectorAll('.datepicker')[0].__vue__.$data.dateSelected = new Date(" + endDate[0] + "," + (Integer.valueOf(endDate[1])-1) + "," + endDate[2] + ")");
    }

    @Then("^the status of the PO should change to \"([^\"]*)\" on RentIt's side$")
    public void the_status_of_the_PO_should_change_to_on_RentIt_s_side(String arg1) throws Throwable {
        driver.get("http://localhost:4010/");
        int count = driver.findElements(By.id("poStatus")).size();
        assertThat(driver.findElements(By.id("poStatus")).get(count-1).getText()).isEqualTo(arg1);
    }

    @Then("^two new actions should appear$")
    public void two_new_actions_should_appear() throws Throwable {
        List<WebElement> elems = driver.findElements(By.id("newActions"));
        assertThat(elems.get(elems.size()-1).findElements(By.tagName("a")).size()).isEqualTo(2);
    }

    @Then("^the end date should change to \"([^\"]*)\"$")
    public void the_end_date_should_change_to(String arg1) throws Throwable {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        driver.get("http://localhost:4010/");
        List<WebElement> elems = driver.findElements(By.id("poEnd"));
        assertThat(elems.get(elems.size()-1).getText()).isEqualTo(arg1);
    }

    @Then("^the extension end date should be \"([^\"]*)\"$")
    public void the_extension_end_date_should_be(String arg1) throws Throwable {
        List<WebElement> elems = driver.findElements(By.id("end"));
        assertThat(elems.get(elems.size()-1).getText()).isEqualTo(arg1);
    }

    @When("^the \"([^\"]*)\" button is pressed next to the created PO$")
    public void the_button_is_pressed_next_to_the_created_PO(String arg1) throws Throwable {
        Thread.sleep(1000);
        driver.get("http://localhost:4010/");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        List<WebElement> elems = driver.findElements(By.id(arg1));
        WebDriverWait wait = new WebDriverWait(driver, 50);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(elems.get(elems.size()-1)));
        element.click();
        if (arg1.equals("return")) {
            List<WebElement> ids = driver.findElements(By.id("poId"));
            invoicedPOid = "PO ID: " + ids.get(ids.size()-1).getText();
        }
    }

    @When("^\"([^\"]*)\" is left as the \"([^\"]*)\"$")
    public void is_left_as_the(String arg1, String arg2) throws Throwable {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        switch (arg2) {
            case "acceptMessage":
                driver.findElement(By.id(arg2)).sendKeys(arg1);
                driver.findElement(By.id("acceptComment")).click();
                Thread.sleep(3000);
                break;
            default:
                driver.findElement(By.id(arg2)).sendKeys(arg1);
                driver.findElement(By.id("rejectComment")).click();
                Thread.sleep(3000);
        }
    }

    @Then("^the status \"([^\"]*)\" and message \"([^\"]*)\" of the PO associated with the PHR should be visible$")
    public void the_status_and_message_of_the_PO_associated_with_the_PHR_should_be_visible(String arg1, String arg2) throws Throwable {
        List<WebElement> elements = driver.findElements(By.id("nameButton"));
        driver.findElements(By.id("nameButton")).get(elements.size()-1).click();
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        Thread.sleep(2000);
        assertThat(driver.findElements(By.id("poMessageText")).get(elements.size()-1).getText()).isEqualTo(arg2);
        assertThat(driver.findElements(By.id("poStatusText")).get(elements.size()-1).getText()).isEqualTo(arg1);
    }

    @Then("^there should be a message \"([^\"]*)\"$")
    public void there_should_be_a_message(String arg1) throws Throwable {
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        assertThat(driver.findElements(By.xpath("//*[contains(text(),'" + arg1 + "')]")).size()).isEqualTo(1);
    }

    @Then("^on BuildIt's side the PHR end date should still be \"([^\"]*)\"$")
    public void on_BuildIt_s_side_the_PHR_end_date_should_still_be(String arg1) throws Throwable {
        driver.get("http://localhost:4000/");
        driver.findElement(By.id("username")).sendKeys("site");
        driver.findElement(By.id("password")).sendKeys("site");
        driver.findElement(By.id("loginButton")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        List<WebElement> elems = driver.findElements(By.id("end"));
        assertThat(elems.get(elems.size()-1).getText()).isEqualTo(arg1);
    }

    @Then("^a \"([^\"]*)\" button should become visible on BuildIt$")
    public void a_button_should_become_visible_on_BuildIt(String arg1) throws Throwable {
        driver.get("http://localhost:4000/");
        driver.findElement(By.id("username")).sendKeys("site");
        driver.findElement(By.id("password")).sendKeys("site");
        driver.findElement(By.id("loginButton")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(By.id(arg1));
        assertThat(elements.get(elements.size()-1).isDisplayed()).isTrue();
    }

    @Then("^a \"([^\"]*)\" button should become visible on RentIt$")
    public void a_button_should_become_visible_on_RentIt(String arg1) throws Throwable {
        driver.get("http://localhost:4010/");
        List<WebElement> elements = driver.findElements(By.id(arg1));
        assertThat(elements.get(elements.size()-1).isDisplayed()).isTrue();
    }

    @When("^the plant is returned$")
    public void the_plant_is_returned() throws Throwable {
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(By.id("returnButton"));
        elements.get(elements.size()-1).click();
    }

    @Then("^the invoice status should be \"([^\"]*)\"$")
    public void the_invoice_status_should_be(String arg1) throws Throwable {
        driver.get("http://localhost:4010/");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(By.id("invoiceButton"));
        elements.get(elements.size()-1).click();

        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        Thread.sleep(3000);
        List<WebElement> elems = driver.findElements(By.id("invoiceStatus"));
        assertThat(elems.get(elems.size()-1).getText()).isEqualTo(arg1);
    }

    @When("^clicks on the \"([^\"]*)\" button$")
    public void clicks_on_the_button(String arg1) throws Throwable {
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(By.id(arg1));
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        elements.get(elements.size()-1).click();
        Thread.sleep(1000);
    }

    @Then("^on BuildIt's site invoice status should be \"([^\"]*)\"$")
    public void on_BuildIt_s_site_invoice_status_should_be(String arg1) throws Throwable {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("http://localhost:4000/");
        driver.findElement(By.id("username")).sendKeys("site");
        driver.findElement(By.id("password")).sendKeys("site");
        driver.findElement(By.id("loginButton")).click();
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        List<WebElement> elems = driver.findElements(By.id("invoiceButton"));
        elems.get(elems.size()-1).click();
        driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(By.id("invoiceStatus"));
        driver.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
        Thread.sleep(4000);
        assertThat(elements.get(elements.size()-1).getText()).isEqualTo(arg1);
    }

    @Then("^the id of the PO associated with the PHR should be correct$")
    public void the_id_of_the_PO_associated_with_the_PHR_should_be_correct() throws Throwable {
        List<WebElement> elements = driver.findElements(By.id("poIdText"));
        assertThat(elements.get(elements.size()-1).getText()).isEqualTo(invoicedPOid);
    }

    @When("^the created plant is dispatched on rentIt$")
    public void the_created_plant_is_dispatched_on_rentIt() throws Throwable {
        WebDriver driver2 = new ChromeDriver();
        driver2.get("http://localhost:4010/");
        driver2.manage().window().maximize();
        ((JavascriptExecutor) driver2).executeScript("window.focus();");
        driver2.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        List<WebElement> elems = driver2.findElements(By.id("dispatchButton"));
        elems.get(elems.size()-1).click();
    }

    @Then("^it should not be possible to cancel the PHR$")
    public void it_should_not_be_possible_to_cancel_the_PHR() throws Throwable {
        Thread.sleep(2000);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        List<WebElement> buttons = driver.findElements(By.id("cancelButton"));
        buttons.get(buttons.size()-1).click();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        assertThat(driver.findElements(By.xpath("//*[contains(text(),'ERROR: PHR cannot be canceled, the status is incorrect!')]")).size()).isEqualTo(1);
    }

    @When("^the works engineer accepts the first created plant hire request$")
    public void the_works_engineer_accepts_the_first_created_plant_hire_request() throws Throwable {
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        List<WebElement> elements = driver.findElements(By.id("acceptButton"));
        elements.get(elements.size()-2).click();
    }
}