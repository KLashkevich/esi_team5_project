package com.buildit.procurement.rest.controller;

import com.buildit.BuilditApplication;
import com.buildit.common.application.dto.BusinessPeriodDTO;
import com.buildit.procurement.application.dto.CreatePoDTO;
import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.application.dto.PurchaseOrderDTO;
import com.buildit.procurement.domain.model.PHRStatus;
import com.buildit.rental.application.dto.PlantInventoryEntryDTO;
import com.buildit.rental.application.services.RentalService;
import com.buildit.rental.domain.model.POStatus;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {BuilditApplication.class,
        ProcurementRestControllerTests.RentalServiceMock.class}, properties = {"spring.main.allow-bean-definition-overriding=true"})
@WebAppConfiguration
@WithMockUser(username = "works", roles={"ADMIN"})
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class ProcurementRestControllerTests {
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Autowired @Qualifier("_halObjectMapper")
    ObjectMapper mapper;

    @Autowired
    RentalService rentalService;

    @Autowired
    RestTemplate restTemplate;

    @Value("${app.rentItUrl}")
    private String rentItUrl;

    private LocalDate testStartDate = LocalDate.now().plusDays(5);
    private LocalDate testEndDate = LocalDate.now().plusDays(10);

    @Configuration
    static class RentalServiceMock {
        @Bean
        public RentalService rentalService() {
            return Mockito.mock(RentalService.class);
        }
        @Bean
        public RestTemplate restTemplate() {
            return Mockito.mock(RestTemplate.class);
        }
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testGetAllPlants() throws Exception {
        Resource responseBody = new ClassPathResource("trucks.json", this.getClass());
        List<PlantInventoryEntryDTO> list =
                mapper.readValue(responseBody.getFile(), new TypeReference<List<PlantInventoryEntryDTO>>() { });
        LocalDate startDate = LocalDate.now();
        LocalDate endDate = startDate.plusDays(2);
        when(rentalService.findAvailablePlants("Truck", startDate, endDate)).thenReturn(list);
        MvcResult result = mockMvc.perform(
                get("/api/procurements/plants?name=Truck&startDate={start}&endDate={end}", startDate, endDate))
                .andExpect(status().isOk())
                .andReturn();

        list = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<PlantInventoryEntryDTO>>() { });
        assertThat(list.size()).isEqualTo(2);
    }

    @Test
    @DirtiesContext
    public void testGetPHRById() throws Exception {
        postTestPlantHireRequest(1L);
        MvcResult result = performGet("/api/procurements/phrs/1");
        PlantHireRequestDTO phr = mapper.readValue(result.getResponse().getContentAsString(),new TypeReference<PlantHireRequestDTO>() { });
        assertThat(phr.get_id()).isEqualTo(1L);

        result = mockMvc.perform(get("/api/procurements/phrs/0"))
                .andExpect(status().isBadRequest())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();
        assertThat(result.getResponse().getContentAsString().contains("does not exist"));
    }

    @Test
    @DirtiesContext
    public void testGetPHRs() throws Exception {
        List<PlantHireRequestDTO> phrs = mapper.readValue(performGet("/api/procurements/phrs")
                .getResponse().getContentAsString(),new TypeReference<List<PlantHireRequestDTO>>() { });
        assertThat(phrs.size()).isEqualTo(0);

        postTestPlantHireRequest(1L);
        postTestPlantHireRequest(2L);
        phrs = mapper.readValue(performGet("/api/procurements/phrs")
                .getResponse().getContentAsString(),new TypeReference<List<PlantHireRequestDTO>>() { });
        assertThat(phrs.size()).isEqualTo(2);

        phrs = mapper.readValue(performGet("/api/procurements/phrs?status=PENDING")
                .getResponse().getContentAsString(),new TypeReference<List<PlantHireRequestDTO>>() { });
        assertThat(phrs.size()).isEqualTo(2);

        phrs = mapper.readValue(performGet("/api/procurements/phrs?status=ACCEPTED")
                .getResponse().getContentAsString(),new TypeReference<List<PlantHireRequestDTO>>() { });
        assertThat(phrs.size()).isEqualTo(0);
    }

    @Test
    @DirtiesContext
    public void testCreatePHR() throws Exception {
        MvcResult result = postTestPlantHireRequest(1L);

        PlantHireRequestDTO newPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        assertThat(newPhr.get_id()).isGreaterThan(0L);
        assertThat(newPhr.getStatus()).isEqualTo(PHRStatus.PENDING);
        assertThat(newPhr.getHiringCost().compareTo(new BigDecimal(1000))).isEqualTo(0);
    }

    @Test
    public void testCreatePHRWrongPeriod() throws Exception {
        PlantInventoryEntryDTO plant = createTestPlant(1L, "Mini excavator", 200);

        ResponseEntity<PlantInventoryEntryDTO> response = new ResponseEntity(plant, HttpStatus.OK);
        doReturn(response).when(restTemplate).getForEntity(rentItUrl + "/plants/1", PlantInventoryEntryDTO.class);

        PlantHireRequestDTO phr = createTestPlantHireRequest("0002", LocalDate.now().minusDays(5), testEndDate, plant);

        MvcResult result = mockMvc.perform(post("/api/procurements/phrs")
                .content(mapper.writeValueAsString(phr))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("startDate must be in the future"));

        phr.setRentalPeriod(BusinessPeriodDTO.of(testStartDate, LocalDate.now().minusDays(20)));

        result = mockMvc.perform(post("/api/procurements/phrs")
                .content(mapper.writeValueAsString(phr))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("startDate must be before endDate"));
        assertThat(result.getResponse().getContentAsString().contains("endDate must be in the future"));
    }

    @Test
    public void testCreatePHRWrongPlant() throws Exception {
        PlantInventoryEntryDTO plant = createTestPlant(0L, null, 0);

        doThrow(new RestClientException("error")).when(restTemplate).getForEntity(rentItUrl + "/plants/0", PlantInventoryEntryDTO.class);

        PlantHireRequestDTO phr = createTestPlantHireRequest("0002", testStartDate, testEndDate, plant);

        MvcResult result = mockMvc.perform(post("/api/procurements/phrs")
                .content(mapper.writeValueAsString(phr))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("Plant inventory entry with id 0 does not exist"));
    }

    @Test
    @DirtiesContext
    public void testAcceptPHR() throws Exception {
        MvcResult result = postTestPlantHireRequest(1L);
        PlantHireRequestDTO newPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        PurchaseOrderDTO po = new PurchaseOrderDTO();
        po.setPoHref("1");
        po.setStatus(POStatus.PENDING);

        CreatePoDTO createPo = CreatePoDTO.of(newPhr.getRentalPeriod(), newPhr.getInventoryEntry());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity entity = new HttpEntity<CreatePoDTO>(createPo, headers);

        ResponseEntity<PurchaseOrderDTO> response = new ResponseEntity(po, HttpStatus.CREATED);
        doReturn(response).when(restTemplate).postForEntity(rentItUrl + "/orders", entity, PurchaseOrderDTO.class);

        result = performPost("/api/procurements/phrs/" + newPhr.get_id() + "/accept");
        newPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        assertThat(newPhr.getStatus()).isEqualTo(PHRStatus.ACCEPTED);
        assertThat(newPhr.getPurchaseOrder()).isNotNull();

        // On a secondary acceptance the error will be thrown
        result = mockMvc.perform(post("/api/procurements/phrs/" + newPhr.get_id() + "/accept"))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("not PENDING"));
    }

    @Test
    public void testAcceptPHRNotFound() throws Exception {
        MvcResult result = mockMvc.perform(post("/api/procurements/phrs/0/accept"))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("not found"));
    }

    @Test
    @DirtiesContext
    public void testRejectPHR() throws Exception {
        MvcResult result = postTestPlantHireRequest(1L);
        PlantHireRequestDTO newPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        result = performDelete("/api/procurements/phrs/" + newPhr.get_id() + "/reject?message=Bad");
        newPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        assertThat(newPhr.getStatus()).isEqualTo(PHRStatus.REJECTED);
        assertThat(newPhr.getComment()).isEqualTo("Bad");

        // On a secondary acceptance the error will be thrown
        result = mockMvc.perform(delete("/api/procurements/phrs/" + newPhr.get_id() + "/reject"))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("not PENDING"));
    }

    @Test
    public void testRejectPHRNotFound() throws Exception {
        MvcResult result = mockMvc.perform(delete("/api/procurements/phrs/0/reject"))
                .andExpect(status().isBadRequest())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("not found"));
    }

    @Test
    @DirtiesContext
    public void testUpdatePHR() throws Exception {
        MvcResult result = postTestPlantHireRequest(1L);
        PlantHireRequestDTO newPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);
        assertThat(newPhr.getStatus()).isEqualTo(PHRStatus.PENDING);
        assertThat(newPhr.getSiteId()).isEqualTo("0001");
        assertThat(newPhr.getHiringCost().compareTo(new BigDecimal(1000))).isEqualTo(0);

        PlantHireRequestDTO patchPhr = new PlantHireRequestDTO();
        patchPhr.setSiteId("0002");
        patchPhr.setRentalPeriod(BusinessPeriodDTO.of(testStartDate.plusDays(1), testEndDate.minusDays(1)));
        result = performPatch("/api/procurements/phrs/1", patchPhr);

        PlantHireRequestDTO updatedPhr = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestDTO.class);

        assertThat(updatedPhr.get_id()).isEqualTo(1L);
        assertThat(updatedPhr.getStatus()).isEqualTo(PHRStatus.PENDING);
        assertThat(updatedPhr.getSiteId()).isEqualTo("0002");
        assertThat(updatedPhr.getRentalPeriod().getStartDate().compareTo(testStartDate.plusDays(1))).isEqualTo(0);
        assertThat(updatedPhr.getRentalPeriod().getEndDate().compareTo(testEndDate.minusDays(1))).isEqualTo(0);
        assertThat(updatedPhr.getHiringCost().compareTo(new BigDecimal(600))).isEqualTo(0);
    }

    @Test
    public void testUpdatePHRNotFound() throws Exception {
        PlantHireRequestDTO patchPhr = new PlantHireRequestDTO();
        patchPhr.setSiteId("0002");
        patchPhr.setRentalPeriod(BusinessPeriodDTO.of(testStartDate.plusDays(1), testEndDate.minusDays(1)));

        MvcResult result = mockMvc.perform(patch("/api/procurements/phrs/0")
                .content(mapper.writeValueAsString(patchPhr))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict())
                .andReturn();

        assertThat(result.getResponse().getContentAsString().contains("not found"));
    }

    private PlantInventoryEntryDTO createTestPlant(Long id, String name, int price) {
        PlantInventoryEntryDTO plant = new PlantInventoryEntryDTO();
        plant.set_id(id);
        plant.setName(name);
        plant.setPrice(new BigDecimal(price));
        return plant;
    }

    private PlantHireRequestDTO createTestPlantHireRequest(String siteId, LocalDate startDate, LocalDate endDate, PlantInventoryEntryDTO plant) {
        PlantHireRequestDTO phr = new PlantHireRequestDTO();
        phr.setSiteId(siteId);
        phr.setRentalPeriod(BusinessPeriodDTO.of(startDate, endDate));
        phr.setInventoryEntry(plant);
        return phr;
    }

    private MvcResult postTestPlantHireRequest(Long plantId) throws Exception {
        PlantInventoryEntryDTO plant = createTestPlant(plantId, "Mini excavator", 200);

        ResponseEntity<PlantInventoryEntryDTO> response = new ResponseEntity(plant, HttpStatus.OK);
        doReturn(response).when(restTemplate).getForEntity(rentItUrl + "/plants/" + plantId, PlantInventoryEntryDTO.class);

        PlantHireRequestDTO phr = createTestPlantHireRequest("0001", testStartDate, testEndDate, plant);

        return performPost("/api/procurements/phrs", phr);
    }

    private  MvcResult performGet(String url) throws Exception {
        MvcResult result = mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(header().string("Location", isEmptyOrNullString()))
                .andReturn();
        return result;
    }

    private MvcResult performPost(String url, Object body) throws Exception {
        MvcResult result = mockMvc.perform(post(url)
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn();
        return result;
    }

    private MvcResult performPost(String url) throws Exception {
        MvcResult result = mockMvc.perform(post(url))
                .andExpect(status().isOk())
                .andReturn();
        return result;
    }

    private MvcResult performDelete(String url) throws Exception {
        MvcResult result = mockMvc.perform(delete(url))
                .andExpect(status().isOk())
                .andReturn();
        return result;
    }

    private MvcResult performPatch(String url, Object body) throws Exception {
        MvcResult result = mockMvc.perform(patch(url)
                .content(mapper.writeValueAsString(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        return result;
    }

}

