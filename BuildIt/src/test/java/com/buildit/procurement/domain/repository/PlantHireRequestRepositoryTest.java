package com.buildit.procurement.domain.repository;

import com.buildit.BuilditApplication;
import com.buildit.procurement.domain.model.PlantHireRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = BuilditApplication.class)
@Sql(scripts="/phr-dataset.sql")
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class PlantHireRequestRepositoryTest {

    @Autowired
    PlantHireRequestRepository phrRepository;

    @Test
    public void findAllRequests() {
        List<PlantHireRequest> requests = phrRepository.findAll();
        assertThat(requests.size()).isEqualTo(8);
    }

    @Test
    public void findRequestsByStatus() {
        List<PlantHireRequest> requests = phrRepository.findPlantHireRequestsByStatus("INVALID");
        assertThat(requests.size()).isEqualTo(0);

        requests = phrRepository.findPlantHireRequestsByStatus("PENDING");
        assertThat(requests.size()).isEqualTo(8);

        requests = phrRepository.findPlantHireRequestsByStatus("ACCEPTED");
        assertThat(requests.size()).isEqualTo(0);

        requests = phrRepository.findPlantHireRequestsByStatus("REJECTED");
        assertThat(requests.size()).isEqualTo(0);
    }
}
