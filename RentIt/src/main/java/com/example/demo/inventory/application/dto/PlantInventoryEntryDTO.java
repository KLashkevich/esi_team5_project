package com.example.demo.inventory.application.dto;

import com.example.demo.common.rest.ResourceSupport;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class PlantInventoryEntryDTO extends ResourceSupport {
    Long _id;
    String name;
    String description;
    BigDecimal price;
}
