package com.example.demo.inventory.application.service;

import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InventoryService {

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    public PlantInventoryEntryDTO findPlantById(Long id) throws Exception {
        PlantInventoryEntry plant = inventoryRepository.findById(id).orElse(null);
        if (plant == null) {
            throw new Exception(String.format("Plant inventory entry with id %s does not exist", id));
        }
        return plantInventoryEntryAssembler.toResource(plant);
    }

    public List<PlantInventoryEntryDTO> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        List<PlantInventoryEntry> entries = inventoryRepository.findAvailablePlants(name, startDate, endDate);
        return plantInventoryEntryAssembler.toResources(entries);
    }

    public List<PlantInventoryItemDTO> findAvailableItems(PurchaseOrderDTO po) {
        List<PlantInventoryItem> items = inventoryRepository.findAvailableItems(po.getPlant().get_id(),
                po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate());
        return plantInventoryItemAssembler.toResources(items);
    }
}
