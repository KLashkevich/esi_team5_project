package com.example.demo.invoicing.application.dto;

import com.example.demo.invoicing.domain.InvoiceStatus;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class InvoiceTransferDTO {
    Long sourceId;
    Long poId;
    BigDecimal totalCost;
    InvoiceStatus status;
    LocalDate dueDate;
}
