package com.example.demo.invoicing.application.service;

import com.example.demo.common.rest.ExtendedLink;
import com.example.demo.invoicing.application.dto.PlantHireInvoiceDTO;
import com.example.demo.invoicing.domain.PlantHireInvoice;
import com.example.demo.invoicing.rest.InvoicingRestController;
import com.example.demo.sales.application.service.PurchaseOrderAssembler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.*;

@Service
public class PlantHireInvoiceAssembler extends ResourceAssemblerSupport<PlantHireInvoice, PlantHireInvoiceDTO> {

    @Autowired
    PurchaseOrderAssembler poAssembler;

    public PlantHireInvoiceAssembler() {
        super(InvoicingRestController.class, PlantHireInvoiceDTO.class);
    }

    @Override
    public PlantHireInvoiceDTO toResource(PlantHireInvoice invoice) {
        PlantHireInvoiceDTO dto = createResourceWithId(invoice.getId(), invoice);
        dto.set_id(invoice.getId());
        dto.setTotalCost(invoice.getTotalCost());
        dto.setDueDate(invoice.getDueDate());
        dto.setStatus(invoice.getStatus());

        dto.setPurchaseOrder(poAssembler.toResource(invoice.getPurchaseOrder()));
        dto.add(new ExtendedLink(
                linkTo(methodOn(InvoicingRestController.class)
                        .findInvoiceById(dto.get_id())).toString(),
                "fetch", GET));
        try {
            switch (invoice.getStatus()) {
                case PENDING:
                    dto.add(new ExtendedLink(
                        linkTo(methodOn(InvoicingRestController.class)
                                .setRemittanceAdvice(dto.get_id())).toString(),
                        "pay", POST));
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(InvoicingRestController.class)
                                    .sendPaymentReminder(dto.get_id())).toString(),
                            "sendReminder", POST));
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(InvoicingRestController.class)
                                    .rejectInvoice(dto.get_id())).toString(),
                            "reject", DELETE));
                    break;
                case REMINDER_SENT:
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(InvoicingRestController.class)
                                    .setRemittanceAdvice(dto.get_id())).toString(),
                            "pay", POST));
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(InvoicingRestController.class)
                                    .rejectInvoice(dto.get_id())).toString(),
                            "reject", DELETE));
                    break;
                default:
                    break;
            }

        } catch (Exception e) {}

        return dto;
    }
}