package com.example.demo.invoicing.rest;

import com.example.demo.invoicing.application.dto.PlantHireInvoiceDTO;
import com.example.demo.invoicing.application.service.InvoicingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/invoicing")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class InvoicingRestController {
    @Autowired
    InvoicingService invoicingService;

    @GetMapping("/invoices")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity findInvoices(@RequestParam(name = "status", required = false) String status) {
        try {
            List<PlantHireInvoiceDTO> invoices = invoicingService.findInvoices(status);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoices);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());

        }
    }

    @GetMapping("/invoices/{id}")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity findInvoiceById(@PathVariable("id") Long id) {
        try {
            PlantHireInvoiceDTO invoice = invoicingService.findInvoice(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoice);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @PostMapping("/orders/{id}/createInvoice")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity createInvoice(@PathVariable("id") Long id) {
        try {
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(invoicingService.createInvoiceForPurchaseOrder(id));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("/invoices/{id}/sendReminder")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity sendPaymentReminder(@PathVariable("id") Long id) {
        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoicingService.sendPaymentReminder(id));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("/invoices/{id}/pay")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity setRemittanceAdvice(@PathVariable("id") Long id) {
        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoicingService.setRemittanceAdvice(id));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @DeleteMapping("/invoices/{id}/reject")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity rejectInvoice(@PathVariable("id") Long id) {
        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(invoicingService.rejectInvoice(id));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }
}
