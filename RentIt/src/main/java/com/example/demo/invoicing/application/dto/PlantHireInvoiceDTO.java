package com.example.demo.invoicing.application.dto;

import com.example.demo.common.rest.ResourceSupport;
import com.example.demo.invoicing.domain.InvoiceStatus;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class PlantHireInvoiceDTO extends ResourceSupport {
    Long _id;
    PurchaseOrderDTO purchaseOrder;
    BigDecimal totalCost;
    InvoiceStatus status;
    LocalDate dueDate;
}
