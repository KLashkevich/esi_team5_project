package com.example.demo.invoicing.domain;

public enum InvoiceStatus {
    PENDING,
    REMINDER_SENT,
    PAID,
    REJECTED
}
