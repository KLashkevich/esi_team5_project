package com.example.demo.invoicing.application.service;

import com.example.demo.invoicing.domain.PlantHireInvoice;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

@Service
public class PlantHireInvoiceValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return PlantHireInvoice.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PlantHireInvoice invoice = (PlantHireInvoice) o;

        if (invoice.getTotalCost() == null) {
            errors.rejectValue("totalCost", "totalCost cannot be null");
        } else if (invoice.getTotalCost().compareTo(BigDecimal.ZERO) <= 0) {
            errors.rejectValue("totalCost", "totalCost must be greater than zero");
        }

        if (invoice.getPurchaseOrder() == null) {
            errors.rejectValue("purchaseOrder", "Invoice should be linked to purchase order");
        }

        if (invoice.getDueDate() == null) {
            errors.rejectValue("dueDate", "dueDate cannot be null");
        }
    }
}
