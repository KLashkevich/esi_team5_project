package com.example.demo.invoicing.application.service;

import com.example.demo.invoicing.application.dto.InvoiceTransferDTO;
import com.example.demo.invoicing.application.dto.PlantHireInvoiceDTO;
import com.example.demo.invoicing.domain.InvoiceStatus;
import com.example.demo.invoicing.domain.PlantHireInvoice;
import com.example.demo.invoicing.domain.PlantHireInvoiceRepository;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.sales.domain.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InvoicingService {

    @Autowired
    PlantHireInvoiceRepository invoiceRepository;

    @Autowired
    PlantHireInvoiceAssembler invoiceAssembler;

    @Autowired
    PlantHireInvoiceValidator invoiceValidator;

    @Autowired
    PurchaseOrderRepository poRepository;

    @Autowired
    ClientHttpRequestFactory clientHttpRequestFactory;

    @Value("${app.buildItInvoicingUrl}")
    private String buildItInvoicingUrl;

    public PlantHireInvoiceDTO findInvoice(Long id) throws Exception {
        PlantHireInvoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice == null) {
            throw new Exception(String.format("Plant hire invoice with id %s does not exist", id));
        }

        return invoiceAssembler.toResource(invoice);
    }

    public List<PlantHireInvoiceDTO> findInvoices(String status) throws Exception {
        List<PlantHireInvoice> invoices;

        if(status != null && !status.isEmpty()) {
            if (isInvoiceState(status)) {
                invoices = invoiceRepository.findPlantHireInvoicesByStatus(status.toUpperCase());
            } else {
                throw new Exception(String.format("Incorrect Status '%s'", status));
            }
        } else {
            invoices = invoiceRepository.findAll();
        }

        return invoiceAssembler.toResources(invoices);
    }

    public PlantHireInvoiceDTO createInvoiceForPurchaseOrder (Long poId) throws Exception {
        PurchaseOrder po = poRepository.findById(poId).orElse(null);
        if (po == null) {
            throw new Exception(String.format("Purchase Order with id %s does not exist", poId));
        }

        if (po.getStatus().equals(POStatus.PLANT_DISPATCHED)) {
            po.setStatus(POStatus.INVOICED);
        }

        LocalDate dueDate = po.getRentalPeriod().getEndDate().plusDays(7); // Assume the due date for payment is week later than return
        PlantHireInvoice invoice = PlantHireInvoice.of(po, dueDate);

        validate(invoice, invoiceValidator);

        invoice = invoiceRepository.save(invoice);
        po.setInvoice(invoice);
        poRepository.save(po);

        InvoiceTransferDTO transferDTO = new InvoiceTransferDTO();
        transferDTO.setSourceId(invoice.getId());
        transferDTO.setPoId(po.getId());
        transferDTO.setDueDate(invoice.getDueDate());
        transferDTO.setStatus(invoice.getStatus());
        transferDTO.setTotalCost(invoice.getTotalCost());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity entity = new HttpEntity<InvoiceTransferDTO>(transferDTO, headers);

        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        ResponseEntity<Object> response =
                restTemplate.postForEntity(buildItInvoicingUrl + "/invoices", entity, Object.class);
        if (response.getStatusCode() != HttpStatus.CREATED) {
            throw new Exception("Invoice creation on customer's end failed");
        }

        return invoiceAssembler.toResource(invoice);
    }

    public PlantHireInvoiceDTO sendPaymentReminder(Long id) throws Exception {
        PlantHireInvoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice == null) {
            throw new Exception(String.format("Plant hire invoice with id %s does not exist", id));
        }

        if (invoice.getStatus() != InvoiceStatus.PENDING) {
            throw new Exception("Payment reminder can be sent only for pending invoices");
        }

        invoice.setStatus(InvoiceStatus.REMINDER_SENT);

        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        ResponseEntity<Object> response =
                restTemplate.postForEntity(
                        buildItInvoicingUrl + "/invoices/" + invoice.getId() + "/setReminder",
                        null, Object.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("Could not send reminder to customer");
        }

        return invoiceAssembler.toResource(invoiceRepository.save(invoice));
    }

    public PlantHireInvoiceDTO setRemittanceAdvice(Long id) throws Exception {
        PlantHireInvoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice == null) {
            throw new Exception(String.format("Plant hire invoice with id %s does not exist", id));
        }

        if (invoice.getStatus() != InvoiceStatus.PENDING && invoice.getStatus() != InvoiceStatus.REMINDER_SENT) {
            throw new Exception("Remittance advice can be sent only for pending invoices and invoices with reminders");
        }

        PurchaseOrder po = invoice.getPurchaseOrder();
        po.setPaymentSchedule(LocalDate.now());
        invoice.setPurchaseOrder(poRepository.save(po));

        invoice.setStatus(InvoiceStatus.PAID);
        return invoiceAssembler.toResource(invoiceRepository.save(invoice));
    }

    public PlantHireInvoiceDTO rejectInvoice(Long id) throws  Exception {
        PlantHireInvoice invoice = invoiceRepository.findById(id).orElse(null);
        if (invoice == null) {
            throw new Exception(String.format("Plant hire invoice with id %s does not exist", id));
        }

        if (invoice.getStatus() != InvoiceStatus.PENDING && invoice.getStatus() != InvoiceStatus.REMINDER_SENT) {
            throw new Exception("Only pending invoices and invoices with reminders can be rejected");
        }

        invoice.setStatus(InvoiceStatus.REJECTED);
        validate(invoice, invoiceValidator);

        return invoiceAssembler.toResource(invoiceRepository.save(invoice));
    }

    private boolean isInvoiceState (String status) {
        for (InvoiceStatus s : InvoiceStatus.values()) {
            if (s.name().contains(status.toUpperCase())) {
                return true;
            }
        }

        return false;
    }

    private void validate(Object o, Validator validator) throws Exception {
        DataBinder dataBinder = new DataBinder(o);
        dataBinder.addValidators(validator);
        dataBinder.validate();
        if (dataBinder.getBindingResult().hasErrors()) {
            throw new Exception(dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode())
                    .collect(Collectors.toList()).toString());
        }
    }

    @Scheduled(cron = "0 0 5 1/1 * ?", zone = "Europe/Tallinn")
    private void sendReminders(){
        List<PlantHireInvoice> all = invoiceRepository.findPlantHireInvoicesByStatus("PENDING");
        List<PlantHireInvoice> dueDatePassed = all.stream()
                .filter(plantHireInvoice -> plantHireInvoice
                        .getDueDate()
                        .isBefore(LocalDate.now()))
                .collect(Collectors.toList());
        dueDatePassed.forEach(plantHireInvoice -> {
            try {
                sendPaymentReminder(plantHireInvoice.getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
