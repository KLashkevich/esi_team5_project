package com.example.demo.invoicing.domain;

import com.example.demo.sales.domain.PurchaseOrder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class PlantHireInvoice {
    @Id
    @GeneratedValue
    Long id;

    @OneToOne
    PurchaseOrder purchaseOrder;

    @Column(precision=8,scale=2)
    BigDecimal totalCost;

    @Enumerated(EnumType.STRING)
    InvoiceStatus status;

    LocalDate dueDate;

    public static PlantHireInvoice of(PurchaseOrder po, LocalDate dueDate) {
        PlantHireInvoice invoice = new PlantHireInvoice();
        invoice.dueDate = dueDate;
        invoice.purchaseOrder = po;
        invoice.totalCost = po.getTotal();
        invoice.status = InvoiceStatus.PENDING;
        return invoice;
    }

    public void setStatus(InvoiceStatus status) {
        this.status = status;
    }

    public void setPurchaseOrder(PurchaseOrder po) {
        this.purchaseOrder = po;
    }
}
