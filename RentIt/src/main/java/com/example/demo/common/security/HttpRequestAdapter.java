package com.example.demo.common.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

import java.io.IOException;
import java.net.URI;

@Configuration
public class HttpRequestAdapter {

    static class MyHttpRequestFactory extends SimpleClientHttpRequestFactory {
        @Autowired
        CredentialsService credentials;

        @Override
        public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
            ClientHttpRequest request = super.createRequest(uri, httpMethod);
            for (String key: credentials.getAuthority().keySet()) {
                if (credentials.getAuthority().get(key).equals(uri.getAuthority())) {
                    request.getHeaders().add("Authorization", credentials.getAuthorization().get(key));
                    break;
                }
            }
            return request;
        }
    }

    @Bean
    public ClientHttpRequestFactory requestFactory() {
        return new MyHttpRequestFactory();
    }
}
