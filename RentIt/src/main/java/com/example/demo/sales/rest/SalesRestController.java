package com.example.demo.sales.rest;

import com.example.demo.common.application.exception.PlantNotFoundException;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.application.service.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/sales")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SalesRestController {
    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    @GetMapping("/plants")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public List<PlantInventoryEntryDTO> findAvailablePlants(
            @RequestParam(name = "name") String plantName,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate
    ) {
        return inventoryService.findAvailablePlants(plantName.toLowerCase(), startDate, endDate);
    }

    @GetMapping("/plants/{id}")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity findPlantById(@PathVariable("id") Long id) {
        try {
            PlantInventoryEntryDTO plant = inventoryService.findPlantById(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(plant);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @GetMapping("/orders")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity findPurchaseOrders(@RequestParam(name = "status", required = false) String status) {
        try {
            List<PurchaseOrderDTO> orders = salesService.findPurchaseOrders(status);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(orders);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @GetMapping("/orders/{id}/items")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity getPurchaseOrderAvailableItems(@PathVariable("id") Long id) {
        try {
            PurchaseOrderDTO po = salesService.findPO(id);

            List<PlantInventoryItemDTO> items = inventoryService.findAvailableItems(po);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(items);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @GetMapping("/orders/{id}")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity fetchPurchaseOrder(@PathVariable("id") Long id) {
        try {
            PurchaseOrderDTO po = salesService.findPO(id);
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(po);
        }
        catch (Exception e) {
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body("ERROR: " + e.getMessage());
        }
    }

    @PostMapping("/orders")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) throws Exception {
        try {
            PurchaseOrderDTO newlyCreatePODTO = salesService.createPO(partialPODTO);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(newlyCreatePODTO.getId().getHref()));
            // The above line won't working until you update PurchaseOrderDTO to extend ResourceSupport

            return new ResponseEntity<>(newlyCreatePODTO, headers, HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("/orders/{id}/accept")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity acceptPurchaseOrder(@PathVariable Long id, @RequestParam(name = "message", required = false) String comment) throws Exception {
        try {
            if(comment.isEmpty()) { comment = " ";}
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(salesService.acceptPO(id, comment));
        } catch (Exception ex) {
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("/orders/{poId}/allocate")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity allocatePurchaseOrder(@PathVariable Long poId, @RequestBody PlantInventoryItemDTO item ) throws Exception {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(salesService.allocatePO(poId, item.get_id()));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ERROR:" + ex.getMessage());
        }
    }

    @DeleteMapping("/orders/{id}/reject")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity rejectPurchaseOrder(@PathVariable Long id, @RequestParam(name = "message", required = false) String comment) throws Exception {
        try {
            if(comment.isEmpty()) { comment = " ";}
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(salesService.rejectPO(id, comment));
        } catch (Exception ex) {
            HttpStatus status = HttpStatus.CONFLICT;
            if (ex.getMessage().toLowerCase().contains("not found"))
                status = HttpStatus.NOT_FOUND;
            return ResponseEntity
                    .status(status)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("/orders/{id}/cancel")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity cancelPurchaseOrder(@PathVariable Long id, @RequestParam(name = "requestNeeded") boolean requestNeeded) throws Exception {
        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(salesService.cancelPurchaseOrder(id, requestNeeded));
        } catch (Exception ex) {
            HttpStatus status = HttpStatus.CONFLICT;
            if (ex.getMessage().toLowerCase().contains("not found"))
                status = HttpStatus.NOT_FOUND;
            return ResponseEntity
                    .status(status)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @DeleteMapping("/orders/{id}/acceptCancel")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity acceptPurchaseOrderCancellation(@PathVariable Long id) throws Exception {
        try {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(salesService.acceptPurchaseOrderCancellation(id));
        } catch (Exception ex) {
            HttpStatus status = HttpStatus.CONFLICT;
            if (ex.getMessage().toLowerCase().contains("not found"))
                status = HttpStatus.NOT_FOUND;
            return ResponseEntity
                    .status(status)
                    .body("ERROR: " + ex.getMessage());
        }
    }

    @DeleteMapping("/orders/{id}")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity closePurchaseOrder(@PathVariable Long id)  {
        try {
            PurchaseOrderDTO po = salesService.closePO(id);

            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(po);
        } catch (Exception ex) {
            String msg = ex.getMessage();
            HttpStatus status = HttpStatus.CONFLICT;
            if (msg.toLowerCase().contains("not found"))
                status = HttpStatus.NOT_FOUND;
            return ResponseEntity
                    .status(status)
                    .body("ERROR: " + msg);
        }
    }

    @PostMapping("orders/{id}/extension")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity requestPoExtension(@PathVariable Long id,
                                             @RequestParam(name = "newEndDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate newEndDate){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(salesService.modifyPO(id, newEndDate));
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("ERROR: "+ ex.getMessage());
        }
    }

    @PostMapping("orders/{id}/acceptExtension")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity acceptExtension(@PathVariable Long id){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(salesService.acceptExtension(id));
        } catch(Exception ex){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("ERROR: " + ex.getMessage());
        }
    }

    @DeleteMapping("orders/{id}/rejectExtension")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity rejectExtension(@PathVariable Long id){
        try {
            return ResponseEntity.status(HttpStatus.OK).body(salesService.rejectExtension(id));
        } catch(Exception ex){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("ERROR: " + ex.getMessage());
        }
    }

    @PostMapping("orders/{id}/dispatch")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public ResponseEntity dispatchPlant(@PathVariable Long id){
        try{
            return ResponseEntity.status(HttpStatus.OK).body(salesService.dispatchPlant(id));
        } catch (Exception ex){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("ERROR: " + ex.getMessage());
        }
    }



    @ExceptionHandler(PlantNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handPlantNotFoundException(PlantNotFoundException ex) {
    }
}
