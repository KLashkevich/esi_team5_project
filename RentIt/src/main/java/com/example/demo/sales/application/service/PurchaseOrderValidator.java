package com.example.demo.sales.application.service;

import com.example.demo.common.application.service.BusinessPeriodValidator;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.math.BigDecimal;

@Service
public class PurchaseOrderValidator implements Validator {

    @Autowired
    BusinessPeriodValidator businessPeriodValidator;

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Override
    public boolean supports(Class<?> aClass) {
        return PurchaseOrder.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        PurchaseOrder po = (PurchaseOrder) o;
        DataBinder binder = new DataBinder(po.getRentalPeriod());
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();
        if (binder.getBindingResult().hasErrors()) {
            errors.rejectValue("rentalPeriod", "Rental period of purchase order is faulty");
        }
        if (plantInventoryEntryRepository.findById(po.getPlant().getId()).orElse(null) == null) {
            errors.rejectValue("plant", "Plant entry on purchase order does not have a valid id");
        }
        if (po.getStatus() == POStatus.OPEN || po.getStatus() == POStatus.CLOSED) {
            if (po.getTotal() == null || po.getTotal().compareTo(new BigDecimal(0)) < 0) {
                errors.rejectValue("total", "Price cannot be negative");
            }
            if (po.getReservations() != null) {
                for (PlantReservation plantReservation : po.getReservations()) {
                    DataBinder pr = new DataBinder(plantReservation.getSchedule());
                    pr.addValidators(new BusinessPeriodValidator());
                    pr.validate();
                    if (pr.getBindingResult().hasErrors()) {
                        errors.rejectValue("schedule", "plant reservation schedule is invalid");
                    }
                }
            } else {
                errors.rejectValue("reservations", "plant reservation is null, but po is open");
            }
        }
        if (po.getStatus() == POStatus.PENDING) {
            if (po.getReservations() != null && !po.getReservations().isEmpty()) {
                errors.rejectValue("reservations", "Pending PO cannot have a plant reservation");
            }
        }
    }
}
