package com.example.demo.sales.application.service;

import com.example.demo.common.application.service.BusinessPeriodValidator;
import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.common.domain.ExtensionPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.sales.domain.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PurchaseOrderRepository poRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    PurchaseOrderValidator purchaseOrderValidator;

    @Autowired
    ClientHttpRequestFactory clientHttpRequestFactory;


    @Value("${app.buildItUrl}")
    private String buildItUrl;

    public PurchaseOrderDTO findPO(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if (po == null) {
            throw new Exception(String.format("Purchase Order with id %s does not exist", id));
        }
        return purchaseOrderAssembler.toResource(po);
    }

    public List<PurchaseOrderDTO> findPurchaseOrders (String status) throws Exception {
        List <PurchaseOrder> orders;

        if(status != null && !status.isEmpty()) {
            if (isPurchaseOrderState(status)) {
                orders = poRepository.findPurchaseOrdersByStatus(status.toUpperCase());
            }
            else {
                throw new Exception(String.format("Incorrect Status '%s'", status));
            }
        }
        else {
            orders = poRepository.findAll();
        }

        return purchaseOrderAssembler.toResources(orders);
    }

    public PurchaseOrderDTO createPO(PurchaseOrderDTO poDTO) throws Exception {

        BusinessPeriod period = BusinessPeriod.of(
                poDTO.getRentalPeriod().getStartDate(),
                poDTO.getRentalPeriod().getEndDate());

        DataBinder binder = new DataBinder(period);
        binder.addValidators(new BusinessPeriodValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors())
            throw new Exception("Invalid PO Period");

        if(poDTO.getPlant() == null)
            throw new Exception("Invalid Input Plant");

        PlantInventoryEntry plant = plantInventoryEntryRepository.findById(poDTO.getPlant().get_id()).orElse(null);

        if(plant == null)
            throw new Exception("Plant NOT Found");

        PurchaseOrder po = PurchaseOrder.of(plant, period);
        List<PlantInventoryItem> availableItems =
                inventoryRepository.findAvailableItems(po.getPlant().getId(), po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate());

        if(availableItems.size() < 1){
            po.setStatus(POStatus.REJECTED);
        }
        long daysToRentFor = DAYS.between(po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate());
        BigDecimal totalPrice = plant.getPrice().multiply(new BigDecimal(daysToRentFor));
        po.setTotal(totalPrice);

        DataBinder dataBinder = new DataBinder(po);
        dataBinder.addValidators(purchaseOrderValidator);
        dataBinder.validate();
        if(dataBinder.getBindingResult().hasErrors()){
            throw new Exception (dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
        }
        PurchaseOrder save = poRepository.save(po);

        return purchaseOrderAssembler.toResource(save);
    }

    public PurchaseOrderDTO acceptPO(Long id, String comment) throws Exception {
        PurchaseOrder po = getPO(id);
        po.setStatus(POStatus.OPEN);
        DataBinder dataBinder = new DataBinder(po);
        dataBinder.addValidators(purchaseOrderValidator);
        dataBinder.validate();
        if(dataBinder.getBindingResult().hasErrors()){
            throw new Exception (dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
        }
        POStatus status = POStatus.OPEN;
        PlantReservation plantReservation = reservePlantInventoryEntry(po);
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        ResponseEntity<Object> response = restTemplate.postForEntity(buildItUrl + "/orders/{oid}/updatepo?status="+status.toString(), comment, Object.class, id);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("PO acceptance failed");
        }
        plantReservationRepository.save(plantReservation);
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    private PlantReservation reservePlantInventoryEntry(PurchaseOrder po) throws Exception {
        PlantReservation plantReservation = new PlantReservation();
        plantReservation.setSchedule(po.getRentalPeriod());
        List<PlantInventoryItem> availableItems =
                inventoryRepository.findAvailableItems(po.getPlant().getId(), po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate());
        if(availableItems.size() >0 ){
            plantReservation.setPlant(availableItems.get(0));
            po.addReservation(plantReservation);
            return plantReservation;
        }
        throw new Exception("There are no available plant items");

    }

    public PurchaseOrderDTO cancelPurchaseOrder(Long id, boolean requestNeeded) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if(po == null) {
            throw new Exception("PO Not Found");
        }

        if(po.getStatus() != POStatus.OPEN && po.getStatus() != POStatus.PENDING) {
            throw new Exception("PO cannot be cancelled due to it its status is incorrect");
        }

        if (!requestNeeded) {
            while (!po.getReservations().isEmpty()) {
                plantReservationRepository.delete(po.getReservations().remove(0));
            }
            po.setStatus(POStatus.CANCELED);
        } else {
            po.setCancellationRequested(true);
        }

        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO acceptPurchaseOrderCancellation(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if(po == null) {
            throw new Exception("PO Not Found");
        }

        if(po.getStatus() != POStatus.OPEN) {
            throw new Exception("PO cannot be cancelled due to it is not Open");
        }

        if(!po.isCancellationRequested()) {
            throw new Exception("There is no cancellation requested");
        }

        while (!po.getReservations().isEmpty()) {
            plantReservationRepository.delete(po.getReservations().remove(0));
        }

        po.setStatus(POStatus.CANCELED);
        po.setCancellationRequested(false);

        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        ResponseEntity<Object> response = restTemplate.postForEntity(
                buildItUrl + "/orders/{oid}/approveCancel", null, Object.class, id);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("PO cancellation acceptance failed");
        }

        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO rejectPO(Long id, String comment) throws Exception {
        PurchaseOrder po = getPO(id);
        while (!po.getReservations().isEmpty()) {
            plantReservationRepository.delete(po.getReservations().remove(0));
        }
        po.setStatus(POStatus.REJECTED);
        DataBinder dataBinder = new DataBinder(po);
        dataBinder.addValidators(purchaseOrderValidator);
        dataBinder.validate();
        if(dataBinder.getBindingResult().hasErrors()){
            throw new Exception (dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
        }
        POStatus status = POStatus.REJECTED;
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        ResponseEntity<Object> response = restTemplate.postForEntity(buildItUrl + "/orders/{oid}/updatepo?status="+status.toString(), comment, Object.class, id);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("PO rejection failed");
        }
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO closePO(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if (po == null)
            throw new Exception("PO Not Found");
        else if(po.getStatus() != POStatus.OPEN)
            throw new Exception("PO cannot be closed due to it is not being Open");
        po.setStatus(POStatus.CLOSED);
        DataBinder dataBinder = new DataBinder(po);
        dataBinder.addValidators(purchaseOrderValidator);
        dataBinder.validate();
        if(dataBinder.getBindingResult().hasErrors()){
            throw new Exception (dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
        }
        poRepository.save(po);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO allocatePO(Long poId, Long itemID) throws Exception {
        PurchaseOrder purchaseOrder = poRepository.findById(poId).orElse(null);
        PlantInventoryItem itemById = inventoryRepository.findItemById(itemID);
        if( validatePoAllocateItem(purchaseOrder, itemById)){
            DataBinder beforeOpening = new DataBinder(purchaseOrder);
            beforeOpening.addValidators(purchaseOrderValidator);
            beforeOpening.validate();
            if(beforeOpening.getBindingResult().hasErrors()){
                throw new Exception (beforeOpening.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
            }

            PlantReservation plantReservation = new PlantReservation();
            plantReservation.setSchedule(purchaseOrder.getRentalPeriod());
            plantReservation.setPlant(itemById);
            purchaseOrder.setStatus(POStatus.OPEN);
            purchaseOrder.addReservation(plantReservation);

            long daysToRentFor = DAYS.between(purchaseOrder.getRentalPeriod().getStartDate(), purchaseOrder.getRentalPeriod().getEndDate());
            BigDecimal totalPrice = itemById.getPlantInfo().getPrice().multiply(new BigDecimal(daysToRentFor));
            purchaseOrder.setTotal(totalPrice);

            DataBinder dataBinder = new DataBinder(purchaseOrder);
            dataBinder.addValidators(purchaseOrderValidator);
            dataBinder.validate();
            if(dataBinder.getBindingResult().hasErrors()){
                throw new Exception (dataBinder.getBindingResult().getFieldErrors().stream().map(error -> error.getCode()).collect(Collectors.toList()).toString());
            }
            plantReservationRepository.save(plantReservation);
            poRepository.save(purchaseOrder);
            return purchaseOrderAssembler.toResource(purchaseOrder);
        }
        throw new Exception ("PO cannot be allocated to this plant");
    }

    private boolean isPurchaseOrderState (String status) {
        for (POStatus s : POStatus.values()) {
            if (s.name().contains(status.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    private PurchaseOrder getPO(Long id) throws Exception {
        PurchaseOrder po = poRepository.findById(id).orElse(null);
        if(po == null)
            throw new Exception("PO Not Found");
        if(po.getStatus() != POStatus.PENDING)
            throw new Exception("PO cannot be accepted/rejected due to it is not Pending");
        return po;
    }

    boolean validatePoAllocateItem(PurchaseOrder po, PlantInventoryItem itemById){
        if(po != null && itemById != null){
            List<PlantInventoryItem> availableItems =
                    inventoryRepository.findAvailableItems(po.getPlant().getId(), po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate());
            return availableItems.stream().filter(item -> Objects.equals(item.getId(), itemById.getId())).findFirst().orElse(null) != null && po.getStatus() == POStatus.PENDING;
        }
        return false;
    }

    public PurchaseOrderDTO modifyPO(Long id, LocalDate newEndDate) throws Exception {
        PurchaseOrder purchaseOrder = poRepository.findById(id).orElse(null);
        if(purchaseOrder == null){
            throw new Exception("PO Not Found");
        }
        if(purchaseOrder.getRentalPeriod().getEndDate().isAfter(newEndDate)){
            throw new Exception("New End date cannot be before end date");
        }
        if(purchaseOrder.getStatus() != POStatus.OPEN ) {
            throw new Exception("PO is not OPEN");
        }
        purchaseOrder.setExtensionPeriod(ExtensionPeriod.of(purchaseOrder.getRentalPeriod().getStartDate(), newEndDate));
        purchaseOrder.setStatus(POStatus.PENDING_EXTENSION);
        poRepository.save(purchaseOrder);
        return purchaseOrderAssembler.toResource(purchaseOrder);
    }

    public PurchaseOrderDTO acceptExtension(Long id) throws Exception {
        PurchaseOrder purchaseOrder = poRepository.findById(id).orElse(null);
        if(purchaseOrder == null){
            throw new Exception("PO Not Found");
        }
        if(purchaseOrder.getStatus() != POStatus.PENDING_EXTENSION){
           throw new Exception("PO STATUS IS NOT PENDING_EXTENSION");
        }
        purchaseOrder.setBusinessPeriod(BusinessPeriod.of(purchaseOrder.getExtensionPeriod().getExtensionStartDate(),
                purchaseOrder.getExtensionPeriod().getExtensionEndDate()));
        purchaseOrder.setExtensionPeriod(null);
        purchaseOrder.setStatus(POStatus.OPEN);

        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        ResponseEntity<Object> response = restTemplate.postForEntity(
                buildItUrl + "/orders/{oid}/updatePOExtension?extended=true", null, Object.class, id);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("PO extension acceptance failed");
        }
        poRepository.save(purchaseOrder);
        return purchaseOrderAssembler.toResource(purchaseOrder);
    }

    public PurchaseOrderDTO rejectExtension(Long id) throws Exception {
        PurchaseOrder purchaseOrder = poRepository.findById(id).orElse(null);
        if(purchaseOrder == null){
            throw new Exception("PO Not Found");
        }
        if(purchaseOrder.getStatus() != POStatus.PENDING_EXTENSION){
            throw new Exception("PO STATUS IS NOT PENDING_EXTENSION");
        }
        purchaseOrder.setExtensionPeriod(null);
        purchaseOrder.setStatus(POStatus.OPEN);
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        ResponseEntity<Object> response = restTemplate.postForEntity(
                buildItUrl + "/orders/{oid}/updatePOExtension?extended=false", null, Object.class, id);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("PO extension rejection failed");
        }
        poRepository.save(purchaseOrder);
        return purchaseOrderAssembler.toResource(purchaseOrder);
    }


    public PurchaseOrderDTO dispatchPlant(Long id) throws Exception {
        PurchaseOrder purchaseOrder = poRepository.findById(id).orElse(null);
        if(purchaseOrder == null ){
            throw new Exception("PO not found");
        }
        if(purchaseOrder.getStatus() != POStatus.OPEN){
            throw new Exception("PO cant be dispatched, because status is not OPEN");
        }
        POStatus status = POStatus.PLANT_DISPATCHED;
        purchaseOrder.setStatus(status);
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
        ResponseEntity<Object> response = restTemplate.postForEntity(buildItUrl + "/orders/{oid}/updatepo?status="+status.toString(), "Plant Has been dispatched", Object.class, id);
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new Exception("Exception");
        }
        PurchaseOrder save = poRepository.save(purchaseOrder);
        return purchaseOrderAssembler.toResource(save);
    }
}
