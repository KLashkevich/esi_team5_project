package com.example.demo.sales.domain;

public enum POStatus {
    PENDING,
    REJECTED,
    OPEN,
    CANCELED,
    PENDING_EXTENSION,
    PLANT_DISPATCHED,
    PLANT_RETURNED,
    INVOICED,
    CLOSED
}
